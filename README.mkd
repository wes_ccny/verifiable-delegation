Verifiable Delegation of Computation
====================================

Overview
--------

Collection of command line utilities and libraries (template source, binary)
which implement protocols for verifiable delegation of computation over large
datasets.  Currently this work is based mainly on the results of [BGV2011].

Dependencies
------------

* [GMP]
* [NTL]
* [OpenSSL]
* [PBC] (optional)

[GMP] is likely already installed on most unix/linux systems, however, it may
be advantageous to compile it yourself, so that the most customized assembly
routines for your machine can be used.  For installation, please follow the
instructions in their readme.  However, we remark that the C++ interface is
required here, and hence you should include `--enable-cxx` amongst the
`./configure` flags, e.g.:

	./configure --enable-cxx


The [NTL] installation procedure is straightforward, however, you will want to
compile it with support for GMP, by passing `NTL_GMP_LIP=on` to `./configure`:

	./configure NTL_GMP_LIP=on

[OpenSSL] is used for standard crypto-primitives, e.g., hash functions, MAC
constructions, etc.

The [PBC] library is currently needed only for implementations based on
elliptic curves.


Building
--------

	autoconf
	./configure [OPTIONS...]
	make

Note that `./configure` has options to support non-system installs of [NTL]
and [PBC] since they aren't all that common (options are
`--with-local-{ntl,pbc}`).  Note also that the generated makefiles have some
additional options beyond what you specified in `./configure` flags, as well
as targets not built by default.  For example, to build the keyword search
application using an elliptic curve group instead of integers, run `make ec`
from directory `keyword/`.


Documentation
-------------

There is currently no manual, but if you have doxygen, run `doxygen Doxyfile`
in the root directory to produce html documentation.  Note that the `Doxyfile`
assumes you have `graphviz` installed.

Caveats
-------

Very little testing has been done.


[BGV2011]: http://eprint.iacr.org/2011/132
[NTL]: http://www.shoup.net/ntl/
[GMP]: http://gmplib.org/
[PBC]: http://crypto.stanford.edu/pbc/
[OpenSSL]: http://www.openssl.org/
