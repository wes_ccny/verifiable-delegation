Overview
========

Code for a simple client / server that run the keyword search protocol.


Building
========

On a linux box, run `make`.  However, you need to be sure that the
prerequisite libraries (in ../primitives/ and ../gmp-utils/) have been built.
There is also an experimental elliptic curve implementation which can be built
using the command `make ec`.  Note also that the elliptic curve implementation
depends on the PBC library: http://crypto.stanford.edu/pbc/


Testing
=======

For now, there is only one "user account" that the server is managing.

To run the code, invent a text file that you want to remotely store.  For
testing, this should probably be kind of small.  Call this file "testfile".
Then to delegate, run

   $ ./server

and from another terminal, run

   $ ./client --init testfile

This will construct a polynomial for keyword search, and delegate it to the
server.  The server's (large) copy of the data is in the file "stashedserver"
by default, and the client's (small) bit of data needed for verification is in
"stashedclient" by default.

Next you will want to test the keyword search.  Run the client with

   $ ./client --seach something

in order to search for "something".  The results will be written to stdout.


TODO's
======

* Let the server manage multiple client accounts (you'll have to add something
  to the initial messages, e.g. the client id!).
* Make search case-insensitive?

# vim:ft=rst
