#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <string>
#include <vector>
using std::string;
using std::vector;
#include <getopt.h>
#include <stdarg.h>

#include "poly-del.hpp"
#include "alg-prf.hpp"
#include "defs.h"

#ifdef USE_EC
#include "ecgroup.h"
typedef ecgroup G;
#else
#include "multgroup.h"
typedef multGrp G;
#endif

int listensock, sockfd, pid;

/* High-level idea: use the polyDel class for the
 * unerlying functionality, and this class to manage
 * the network transfer, wrapper, etc. */

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int setup()
{
	/* first read # of hash table bins */
	size_t k;
	read(sockfd,&k,sizeof(size_t));
	size_t nBins = (1 << k);
	FILE* f = fopen("stashedserver","wb");
	int fd = fileno(f);
	write(fd,&nBins,sizeof(size_t)); /* save nBins */
	/* we will write the ssindex file with integers corresponding
	 * to the file positions (in bytes) of the start of each polyDel
	 * stash.  Each integer will be precisely 8 bytes, to enable random
	 * access into the index. */
	FILE* fMeta = fopen("ssindex","wb");
	size_t cpos;
	for (size_t i = 0; i < nBins; i++) {
		fflush(f); /* sync fd with f. */
		cpos = ftell(f);
		fwrite(&cpos,sizeof(size_t),1,fMeta);
		polyDel<G> myCopy;
		myCopy.unstash(sockfd);
		myCopy.stash(fd);
	}
	fclose(f);
	fclose(fMeta);
	return 0;
}

/* respond to client's query */

int search()
{
	ZZ_p x,y;
	G t;
	/* first read the hash of the keyword from the network. */
	zz_from_file(sockfd,x);
	FILE* f = fopen("stashedserver","rb");
	int fd = fileno(f);
	/* read nBins first... */
	size_t nBins;
	read(fd,&nBins,sizeof(size_t));
	size_t lmask = nBins - 1;
	size_t bIndex;
	conv(bIndex,x);
	bIndex &= lmask;
	/* look up address from the index. */
	FILE* fMeta = fopen("ssindex","rb");
	fseek(fMeta, bIndex * sizeof(size_t), SEEK_SET);
	size_t stashLoc;
	fread(&stashLoc, sizeof(size_t), 1, fMeta);
	fclose(fMeta);
	/* FIXME: overly careful? */
	fflush(f);
	fseek(f,stashLoc,SEEK_SET);
	fflush(f); /* sync fd with f */
	polyDel<G> D;
	D.unstash(fd);
	/* FIXME: can you do this all with the stream f now?
	 * I don't think you really need fd anymore for this. */
	fclose(f);

	D.compute(x,y,t);
	/* send them back: */
	zz_to_file(sockfd,y);
	t.to_file(sockfd);
	return 0;
}


/* network stuff... */

int networkActions()
{
	int curOp;
	int n = read(sockfd, &curOp, sizeof(int)); // ssize_t might be better than int.
	if (n < 0)
		error("ERROR reading from socket\n");
	switch (curOp)
	{
		case upload:
			setup();
			break;
		case query:
			search();
			break;
		default:
			fprintf(stderr, "Unknown operation %d\n", curOp);
			return -1;
	}
	#if 0
	unsigned char dummy[64];
	ssize_t r;
	do
	{
		r = recv(sockfd, dummy, 64, 0);
	}while (r != 0 && r != -1);
	#endif
	close(sockfd);
	return 0;
}

void networkEngine()
{
	// We'll allow a max of 1000 consecutive connections.
	// Not that this has any importance, but maybe establishing a limit
	// might be a good idea...for now.
	socklen_t clilen;
	struct sockaddr_in  cli_addr;
	int limit = 0;
	int status;
	while (limit != 1000)
	{
		sockfd = accept(listensock, (struct sockaddr *) &cli_addr, &clilen);
		if (sockfd < 0)
			error("ERROR on accept\n");
		pid = fork();
		if (pid < 0)
			error("ERROR on fork\n");
		if (pid == 0)
		{
			close(listensock);
			networkActions();
			exit(0);
		}
		else close (sockfd);
		limit++;
		waitpid(-1, &status, WNOHANG);
	}
}

int initNetwork(char* hostname, int port)
{
	int reuse = 1;
	struct sockaddr_in serv_addr;
	listensock = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listensock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
	/* TODO: I think if you make sure the client closes first,
	 * this won't be an issue. */
	if (listensock < 0)
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if (bind(listensock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR on binding");
	listen(listensock,5);
	networkEngine();
	return 0;
}

int shutdownNetwork()
{
	unsigned char dummy[64];
	ssize_t r;
	do {
		r = recv(sockfd,dummy,64,0);
	} while (r != 0 && r != -1);
	close(sockfd);
	close(listensock);
	return 0;
}

/* end network stuff. */

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Run keyword search protocol (server).\n\n"
"   --port       PORT      listen on PORT. defaults to %lu\n"
"   --host       HOST      connect to HOST. default is %s\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[])
{
	static int help=0;
	static struct option long_opts[] = {
		{"port",       required_argument, 0, 'p'},
		{"host",       required_argument, 0, 'h'},
		{"help",       no_argument,       &help,    1},
		{0,0,0,0} // this denotes the end of our options.
	};
	int c; // holds an option
	int opt_index = 0;
	int port = 31337;
	char hostname[FNAME_MAX_LEN] = "localhost";
	/* ensure these are c-strings.  set null char, and don't
	 * copy more than len-1 chars */
	hostname[FNAME_MAX_LEN-1] = 0;
	while ((c = getopt_long(argc, argv, "p:h:",
					long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'p':
				port = atoi(optarg);
				break;
			case 'h':
				strncpy(hostname,optarg,FNAME_MAX_LEN-1);
				break;
			case '?':
				return 1;
		}
	}

	if (help) {
		printf(usage,argv[0], port, hostname);
		return 0;
	}

	int rcode = 0;

	algPRF<G>::init();
	initNetwork(hostname,port);
	close(listensock);
	close(sockfd);

	return rcode;
}
