datafiles=( http://www.gutenberg.org/cache/epub/1064/pg1064.txt \
			http://www.gutenberg.org/cache/epub/932/pg932.txt \
			http://www.gutenberg.org/cache/epub/10031/pg10031.txt )

# suffix for test files:
suffix="-test"

print_usage()
{
cat <<EOF
usage: run-tests.sh [OPTIONS] [DIR]

Run some tests using files in DIR with suffix "-test".  Test files
will be downloaded to a temp directory if DIR is not supplied.

OPTIONS:
   -h         Print this message.
   -t         Be terse.  Print rows of tab-separated data as follows:
              <#bytes><#words><#uniqewords><upload time><search time> \\
                   <server storage><client storage>
   -r FILE    Fabricate test input via random subsets of lines in FILE
   -c NUM     Run NUM tests (only applies to -r)
   -D         Remove test files afterward.
EOF
}

terse=0
rmtestfiles=0
randwords=""
tcount=3
while getopts "htr:Dc:" OPTION
do
	case $OPTION in
	h)
		print_usage
		exit 0
		;;
	t)
		terse=1
		;;
	r)
		randwords=$OPTARG
		;;
	c)
		tcount=$OPTARG
		;;
	D)
		rmtestfiles=1
		;;
	?)
		print_usage
		exit 1
		;;
	esac
done
shift $(($OPTIND - 1))

# if $1 is specified, use that as the temp directory.  else use /tmp/
if [[ -z $1 ]]; then
	testdir=$(mktemp -d "/tmp/kwtests-XXXXX")
	removedir=1
else
	testdir=$1
	removedir=0
fi

if [[ -n $randwords ]]; then
	for (( i = 0; i < $tcount; i++ )); do
		awk "BEGIN{srand()} {if (rand() < 1 - 1.0/(2+$i)) {print \$0}}" \
			< $randwords > $testdir/$i$suffix
	done
else
	for (( i = 0; i < ${#datafiles[@]}; i++ )); do
		[[ ! -f $testdir/$i$suffix ]] && curl "${datafiles[$i]}" > \
			$testdir/$i$suffix
	done
fi

# now run some timing tests.  First get the server up:
set -m
(cd .. && ./server &)

for f in $testdir/*$suffix ; do
	bytes=$(wc -c < $f)
	words=$(wc -w < $f)
	uwords=$(awk '{for (i=1; i<=NF; i++) x[$i]++} END{print length(x)}' < $f)
	# now run the client and wait.
	cd ..
	ctime=$( (time ./client --init $f 1>&/dev/null ) 2>&1)
	cd - &> /dev/null

	# TODO: The server time is not yet measured, in part since it is running
	# continuously for multiple client requests.
	# TODO: The time for searches has not yet been measured either.
	# TODO: I think it might be easiest to add a debugging mode to the client
	# and the server which would print out some messages about what it did,
	# and how long it took.  On the other hand, the time function gives you
	# the syscall vs userspace breakdown...
	# TODO: Do we need the storage here?  We can compute it precisely if
	# given the number of unique words in the file.

	# let's start with wall-clock for time
	ctimeReal=$(awk '/real/ {print $2}' <<<"$ctime")
	# NOTE: we'll truncate to seconds if we have spilled into minutes
	# (bash doesn't do floating point arithmetic, and adding bc as a
	# dependency of this thing seems dumb)
	if (( ${ctimeReal/%m*/} != 0 )); then
		ctimeReal=$((${ctimeReal/%m*/}*60 + ${ctimeReal:2:1}))
	else
		ctimeReal=${ctimeReal:2:5}
	fi
	if (( $terse == 0 )); then
		echo "Processing $f"
		echo -e "   bytes:\t$bytes"
		echo -e "   words:\t$words ($uwords unique)"
		echo -e "   client times:\t"$ctime
	else
		echo -e "$bytes\t$words\t$uwords\t"$ctimeReal
	fi
done

# kill server.  Can't use $! since it was started in a subshell.
pkill -u $(whoami) -x 'server'
wait

if (( $rmtestfiles == 1 )); then
	if (( $removedir == 1 )); then
		rm -r $testdir
		exit 0
	fi
	# otherwise, just kill the files:
	rm $testdir/*$suffix
fi
