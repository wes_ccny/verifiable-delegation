#!/bin/bash
## Gather specifications
#echo "Enter how many words per string size you want (enter 'd' for default):"  ; read quantity
#echo "Enter smallest word size (enter 'd' for default):"                       ; read begin_size
#echo "Enter largest word size (enter 'd' for default):"                        ; read end_size
#echo "Enter increment (enter 'd' for default):"                                ; read increment
#[ "$quantity"   == "d" ] && quantity=10000
#[ "$begin_size" == "d" ] && begin_size=1
#[ "$end_size"   == "d" ] && end_size=2056
#[ "$increment"  == "d" ] && increment=1
#echo "You have selected a frequency of $frequency between word sizes of $begin_size and $end_size"
## echo "Please make sure you have enough free space"
## End specifications

quantity=1000
begin_size=1
end_size=2056
increment=1

for i in $(eval echo {$begin_size..$end_size..$increment})
do
	echo "$i of [$end_size] -- increment = $increment"
	./randwords --num $quantity --len $i > $i.$quantity.txt
done
