/* Generates random words for testing keyword search app. */
#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <cstdlib>
#include <ctime>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Generate some random words and write them to stdout.\n"
"(Note that these are not of cryptographic quality.)\n\n"
"   --num    NUM    generate NUM words.  Defaults to %lu\n"
"   --len    NUM    make each word NUM chars.  Defaults to %lu\n"
"   --help          show this message and exit.\n";

static const char* chars = "abcdefghijklmnopqrstuvwxyz"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#";
/* NOTE: we made this 64 chars for efficiency.
 * The last two are admittedly a little strange. */
#define ALPHABITS 6 /* chars should have length 2**ALPHABITS */

int main(int argc, char *argv[]) {
	// define long options
	size_t count = 1000;
	size_t len = 9;
	static struct option long_opts[] = {
		{"help",     no_argument,       0, 'h'},
		{"num",      required_argument, 0, 'c'},
		{"len",      required_argument, 0, 'l'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hc:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],count,len);
				return 0;
			case 'c':
				count = atol(optarg);
				break;
			case 'l':
				len = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],count,len);
				return 1;
		}
	}

	srand((unsigned int) time(0));
	/* NOTE: This may affect portability:
	 * Assuming RAND_MAX is 2**N-1, and assuming the distribution on
	 * [0..RAND_MAX] is uniform, then for any integer t <= N, each
	 * t-bit chunk of rand() output is a uniform bit string.
	 * E.g., with gcc, we can get 31/6 = 5 random characters (see above
	 * chars array) from every invocation of rand(). */
	size_t N = 0;
	/* expensive version of x64 BSR T.T */
	int tmp = RAND_MAX;
	while (tmp) {
		tmp >>= 1;
		N++;
	}
	size_t nBlocks = N / ALPHABITS;
	char* word = new char[len+1];
	const int LMASK = (1 << ALPHABITS) - 1;
	word[len] = 0;
	int R;
	size_t t; /* target index into word. */
	for (size_t i = 0; i < count; i++) {
		t = 0;
		for (size_t j = 0; j < len/nBlocks; j++) {
			R = rand();
			for (size_t k = 0; k < nBlocks; k++) {
				word[t++] = chars[R & LMASK];
				R >>= ALPHABITS;
			}
		}
		/* now just need one more rand call. */
		R = rand();
		while (t < len) {
			word[t++] = chars[R & LMASK];
			R >>= ALPHABITS;
		}
		printf("%s\n",word);
	}
	delete [] word;
	return 0;
}
