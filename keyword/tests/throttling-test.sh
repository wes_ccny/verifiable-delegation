#!/bin/bash
#./make-random-words.sh

# EDIT: timeout script has been added to account
# for threaded situation. We unfortunately cannot
# escape it.

# Get current and parent directory
current_dir=$(pwd)
parent_dir="$(dirname "$current_dir")"

# Server app should be running on a different terminal
# window to prevent throttling of that system.

# Set up throttling options
#     NOTE: This is assuming the client doesn't
#     fork any processes. This implies that we are NOT
#     implementing multithreadding on the phone (once we
#     actually port it to Android, then we go ahead and
#     do it without a problem)

# Data seg size, max memory size, and virtual memory
# set to 1 GB, which should do fine in replicating
# a Galaxy S3 phone, which uses YAFFS2 file system.
# ulimit -d 1000000 -m 1000000 -v 1000000
# ulimit -a

# Hard drive
#     NOTE: Hard drive has no need for throttling
#     as most smartphones have pretty good
#     read/write speeds given that they use flash
#     drives, so they are actually faster than most
#     computers in that particular regard.

# Network
#     NOTE: Network throttling in this case would
#     also be completely unecessary for query, as
#     very little is transferred over the network.

# START TESTS; you can change these values depending
# on your specifications. The default ones should
# do fine though.

quantity=1000
begin_size=1
end_size=2056
increment=1
search_increment=5

## CPU 1.4 Ghz Cortex-A9 simulator.
# cpulimit -P $parent_dir/client -c 1 -l 60

for i in $(eval echo {$begin_size..$end_size..$increment})
do
    echo "$i of [$end_size] -- increment = $increment"
    ./randwords --num $quantity --len $i > $i.$quantity.txt
    for j in $(eval echo {$begin_size..$end_size..$search_increment})
    do
        echo "$i.$quantity.txt" >> time_results.txt
        random_word=$(sed -n "$(echo $j)p" $i.$quantity.txt)
        echo "Chosen word: $random_word" >> time_results.txt
        time $parent_dir/client --search $random_word >> time_results.txt
    done
done

