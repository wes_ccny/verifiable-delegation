suffix="-test"

print_usage()
{
cat <<EOF
usage: run-tests.sh [OPTIONS] [DIR]

Run some tests using files in DIR with suffix "-test".  Test files
will be downloaded to a temp directory if DIR is not supplied.

OPTIONS:
   -h         Print this message.
   -t         Be terse.  Print rows of tab-separated data as follows:
              <#uniqewords><upload time>
   -c NUM     Run NUM tests
   -s NUM     Size of first test (smallest, measured in words)
   -i NUM     Size increment between tests
   -D         Remove test files afterward.
EOF
}

terse=0
rmtestfiles=0
tcount=7
tsize=500
tinc=500
while getopts "htDc:s:i:" OPTION
do
	case $OPTION in
	h)
		print_usage
		exit 0
		;;
	t)
		terse=1
		;;
	c)
		tcount=$OPTARG
		;;
	s)
		tsize=$OPTARG
		;;
	i)
		tinc=$OPTARG
		;;
	D)
		rmtestfiles=1
		;;
	?)
		print_usage
		exit 1
		;;
	esac
done
shift $(($OPTIND - 1))

# if $1 is specified, use that as the temp directory.  else use /tmp/
if [[ -z $1 ]]; then
	testdir=$(mktemp -d "/tmp/kwtests-XXXXX")
	removedir=1
else
	testdir=$1
	removedir=0
fi

# now run some timing tests.  First get the server up:
set -m
(cd .. && ./server &)
sleep .3   # make sure the server is up.

for (( i = 0; i < $tcount; i++ )); do
	f=$testdir/$i$suffix
	./randwords --num $tsize > $f
	# with good probability (tunable via ./randwords --len N) each of the
	# words will be unique.
	uwords=$tsize
	((tsize+=tinc))

	# now run the client and wait.
	cd ..
	ctime=$( (time ./client --init $f 1>&/dev/null ) 2>&1)
	cd - &> /dev/null

	# let's start with wall-clock for time
	ctimeReal=$(awk '/real/ {print $2}' <<<"$ctime")
	# NOTE: we'll truncate to seconds if we have spilled into minutes
	# (bash doesn't do floating point arithmetic, and adding bc as a
	# dependency of this thing seems dumb)
	if (( ${ctimeReal/%m*/} != 0 )); then
		ctimeReal=$((${ctimeReal/%m*/}*60 + ${ctimeReal:2:1}))
	else
		ctimeReal=${ctimeReal:2:5}
	fi
	if (( $terse == 0 )); then
		echo "Processing $f"
		echo -e "   unique words:\t$uwords"
		echo -e "   client times:\t"$ctime
	else
		echo -e "$uwords\t$ctimeReal"
	fi
done

# kill server.  Can't use $! since it was started in a subshell.
pkill -u $(whoami) -x 'server'
wait

if (( $rmtestfiles == 1 )); then
	if (( $removedir == 1 )); then
		rm -r $testdir
		exit 0
	fi
	# otherwise, just kill the files:
	rm $testdir/*$suffix
fi
