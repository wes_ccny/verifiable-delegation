#!/bin/bash

# run a test and plot it.

# tmpresults=$(mktemp /tmp/trsltXXXXX)
# use static file for now.
tmpresults=/tmp/tkwresults
tmpplot=/tmp/kwplot.png

./simple-test.sh -D -t -s 1000 -i 1000 > $tmpresults
# if you want to keep the data for debugging don't use -D:
# ./simple-test.sh -t -s 1000 -i 1000 > $tmpresults

if [[ -z $1 ]]; then
	gnuplot -p -e "plot '$tmpresults' using 1:2 with linespoints"
else
	gnuplot -e "set terminal png ; set output '$tmpplot' ; plot '$tmpresults' using 1:2 with linespoints"
fi

