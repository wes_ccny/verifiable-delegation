#include <QtGui>
#include "mainwindow.h"
#include <string>
using std::string;
#include <cstdio>

MainWindow::MainWindow()
{
	setWindowTitle(tr("QtTest"));
	this->kwds = new QTextEdit;
	this->searchButton = new QPushButton("&Search");
	this->host = new QLineEdit;
	this->port = new QLineEdit;
	this->host->setText("localhost");
	this->port->setText("31337");
	QHBoxLayout* topl = new QHBoxLayout;
	QFormLayout* forml = new QFormLayout;
	QVBoxLayout* layout = new QVBoxLayout;
	forml->setHorizontalSpacing(10);
	forml->setVerticalSpacing(5);
	forml->addRow("&Host",this->host);
	forml->addRow("&Port",this->port);
	layout->addWidget(kwds);
	layout->addWidget(searchButton);
	topl->addItem(forml);
	topl->addItem(layout);
	this->setLayout(topl);
	QObject::connect(searchButton, SIGNAL(clicked()), this, SLOT(search()));
	resize(400, 150);
}

void MainWindow::search()
{
	string s = this->kwds->toPlainText().toStdString();
	QString kw = this->kwds->toPlainText();
	/* build the command to run */
	QString cmd = "cd .. && ./client --terse --host " + this->host->text()
		+ " --port " + this->port->text() + " --search " + kw;
	char* cmd_cstr = cmd.toAscii().data();
	FILE* p = popen(cmd_cstr,"r");
	if (!p) {
		fprintf(stderr, "Failed to run command `%s`\n",cmd_cstr);
		return;
	}
	int found, verified;
	if (fscanf(p,"%i\t%i\n",&found,&verified) != 2) {
		fprintf(stderr, "Couldn't parse output...\n");
		fprintf(stderr, "found: %i\tverified: %i\n",found,verified);
	}
	fclose(p);
	//this->kwds->clear();
	int ofont = this->kwds->fontWeight();
	QColor ocolor = this->kwds->textColor();
	this->kwds->setFontWeight(QFont::DemiBold);
	if (found) {
		this->kwds->setTextColor(Qt::darkGreen);
		this->kwds->append("  :: found.");
	} else {
		this->kwds->setTextColor(Qt::darkBlue);
		this->kwds->append("  :: NOT found.");
	}
	if (verified) {
		this->kwds->setTextColor(Qt::darkGreen);
		this->kwds->append("  (Answer was verified.)");
	} else {
		this->kwds->setTextColor(Qt::red);
		this->kwds->append("  (Answer NOT VERIFIED.)");
	}
	this->kwds->setFontWeight(ofont);
	this->kwds->setTextColor(ocolor);
}

