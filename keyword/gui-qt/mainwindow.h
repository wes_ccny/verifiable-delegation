#pragma once
#include <QtGui>

class MainWindow : public QWidget
{
	Q_OBJECT

public:
	MainWindow();

private slots:
	void search();

private:
	QTextEdit* kwds;
	QLineEdit* host;
	QLineEdit* port;
	QPushButton* searchButton;
};
