#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <tr1/unordered_set>
#include <string>
#include <vector>
using std::string;
using std::tr1::unordered_set;
using std::vector;

#include <getopt.h>
#include <stdarg.h>

#include "gmp-ntl-conv.h"
#include "poly-del.hpp"
#include "alg-prf.hpp"
#include "defs.h"
#include "prf.h"

#ifdef USE_EC
#include "ecgroup.h"
typedef ecgroup G;
#else
#include "multgroup.h"
typedef multGrp G;
#endif

/* this is the desired maximum degree; the actual maximum will
 * usually be a bit larger due to hash collisions. */
#ifndef MAXDEG
#define MAXDEG 256
#endif

/* High-level idea: use the polyDel class for the
 * unerlying functionality, and this class to manage
 * the network transfer, wrapper, etc. */

/* network stuff... */

int sockfd;

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

int initNetwork(char* hostname, int port)
{
	struct sockaddr_in serv_addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	struct hostent *server;
	if (sockfd < 0)
		error("ERROR opening socket");
	server = gethostbyname(hostname);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
	serv_addr.sin_port = htons(port);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
		error("ERROR connecting");
	/* connection now ready to send / recv. */
	return 0;
}

int shutdownNetwork()
{
	shutdown(sockfd,2);
	unsigned char dummy[64];
	ssize_t r;
	do {
		r = recv(sockfd,dummy,64,0);
	} while (r != 0 && r != -1);
	close(sockfd);
	return 0;
}

/* end network stuff. */

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Run keyword search protocol (client).\n\n"
"   --port       PORT      connect on PORT. defaults to %lu\n"
"   --host       HOST      connect to HOST. default is %s\n"
"   --init       FILE      upload file FILE\n"
"   --search     WORD      searches server for WORD\n"
"   --terse                0/1 values for search result + verification.\n"
"   --help                 show this message and exit.\n";

/* setup the delegation so that we can search for keywords
 * in a file. */
/* construct / delegate the polynomial $prod_{w\in S} (x-H(w))$.
 * NOTE: we'll actually delegate many polynomials so that each has
 * expected degree MAXDEG.  For this, we use the LSB's of H(w)
 * as the hash. */
int setup(char* fname)
{
	typedef unordered_set<string> HS;
	HS S;
	FILE* f;
	f = fopen(fname,"rb");
	char* line = 0;
	char* token;
	size_t len;
	ssize_t read;
	char delims[] = " ,.:;_\n\r\t*-=()";
	while ((read = getline(&line,&len,f)) != -1) {
		token = strtok(line,delims);
		while(token) {
			S.insert(token);
			token = strtok(NULL,delims);
		}
	}
	free(line);
	fclose(f);

	/* Setup private keys for HMAC and for the PRF... */
	unsigned char hmkey[32];
	unsigned char prfseed[32];
	f = fopen("/dev/urandom","rb");
	fread(hmkey,1,32,f);
	fread(prfseed,1,32,f);
	fclose(f);
	PRFSetSeed(prfseed,32); /* init the prf with our secret key. */
	unsigned char* output; /* hmac output */

	/* compute number of hash table bins */
	size_t nBins = S.size() / MAXDEG;
	/* now from nBins, compute the number of bits (k) of hmac needed */
	size_t k=0;
	while (nBins) {
		k++;
		nBins >>= 1;
	}
	/* NOTE: k could be 0. */
	/* now redefine nBins with this approximation */
	nBins = (1 << k);
	size_t lmask = nBins - 1;
	/* NOTE: somehow an STL vector vec_ZZ_p goes all kinds of wrong. */
	vec_ZZ_p* roots = new vec_ZZ_p[nBins];

	ZZ tmp;
	ZZ_p hashv; // store hash as an integer.
	HS::iterator it;
	for (it=S.begin(); it!= S.end(); it++) {
		output = HMAC(EVP_sha1(),hmkey,32, (const unsigned char*)it->c_str(),
				it->length(),NULL,NULL);
		/* now convert to mpz_t, and multiply into the accumulator. */
		ZZFromBytes(tmp,output,20);
		conv(hashv,tmp);
		/* get the last k bits and use this for the hash */
		/* TODO: make sure conv() is doing what you expect here. */
		size_t bIndex;
		conv(bIndex,hashv);
		append(roots[bIndex & lmask],hashv);
	}

	/* compute the maximal degree; we must make all of them
	 * the same to avoid linear storage...  */
	size_t maxDegree = 0;
	for (size_t i = 0; i < nBins; i++) {
		if (maxDegree < (size_t)roots[i].length())
			maxDegree = roots[i].length();
	}
	/* now make each polynomial to be of the same degree
	 * by adding a factor of x^n. */
	ZZ_p zerop; /* :\ */
	conv(zerop,ZZ::zero());
	for (size_t i = 0; i < nBins; i++) {
		for (size_t j = roots[i].length(); j < maxDegree; j++)
			append(roots[i],zerop);
	}

	/* first have to let the server know what we want to do: */
	int curOp = upload;
	int n = write(sockfd,&curOp,sizeof(int));
	if (n < 0) error("ERROR writing operation to socket\n");
	/* communicate the number of bins (via k) */
	n = write(sockfd,&k,sizeof(size_t));
	if (n < 0) error("ERROR writing operation to socket\n");

	/* now loop through the hash table... */
	ZZ_pX P;
	RNG_CTX ctx;
	unsigned char prfoutput[16];
	for (size_t i = 0; i < nBins; i++) {
		clear(P);
		BuildFromRoots(P,roots[i]);
		polyDel<G> D(P);
		/* use prf to get a context: */
		PRF(i,prfoutput);
		init_RNG_CTX(ctx,prfoutput,16);
		D.keyGen(&ctx);
		/* save the public part in pPub, and clear out the arrays
		 * from our own copy: */
		polyDel<G> pPub;
		D.split(pPub);
		pPub.stash(sockfd); /* send public part to the server */
	}

	delete[] roots;

	/* now write our (small) verification info locally: */
	f = fopen("stashedclient","wb");
	if (!f) {
		fprintf(stderr, "Couldn't open file stashedclient\n");
		return -1;
	}
	/* NOTE: we no longer need to save the polyDel instances,
	 * since we can re-generate them from the PRF seed.
	 * NOTE: however, you will need the degrees of the polynomials,
	 * which cannot be recovered from the PRF x_x  Solution: make
	 * every polynomial of the same, maximal degree by appending a
	 * factor of x^{D-d} */
	int fd = fileno(f);
	write(fd,hmkey,32);
	write(fd,prfseed,32);
	write(fd,&k,sizeof(size_t));
	write(fd,&maxDegree,sizeof(size_t)); /* save degree... T.T */
	/* okay, so we have everything we need to recover all this later
	 * on: the hmac key will let us perform searches, the prfseed
	 * will let us recover all of the secret keys for the polyDel's,
	 * and the degree of each one will always be maxDegree. */
	//D.stash(fd);
	fclose(f);
	return 0;
}

int search(char* kw, int terse=0)
{
	/* tell server we want to search: */
	int curOp = query;
	int n = write(sockfd,&curOp,sizeof(int));
	if (n < 0) error("ERROR writing operation to socket\n");

	/* load our secret keys for the MAC and polydel. */
	unsigned char hmkey[32];
	unsigned char prfseed[32];
	size_t maxDegree;
	size_t k;
	FILE* f = fopen("stashedclient","rb");
	if (!f) {
		fprintf(stderr, "Couldn't open file stashedclient\n");
		return -1;
	}
	int fd = fileno(f);
	read(fd,hmkey,32);
	read(fd,prfseed,32);
	read(fd,&k,sizeof(size_t));
	read(fd,&maxDegree,sizeof(size_t));
	fclose(f);
	size_t nBins = (1 << k);
	size_t lmask = nBins - 1;
	PRFSetSeed(prfseed,32);

	/* compute hmac(kw) */
	unsigned char* output; /* will be a 20 byte buffer after hmac call */
	output = HMAC(EVP_sha1(),hmkey,32, (const unsigned char*)kw,
			strnlen(kw,KEYWORD_MAX_LEN),NULL,NULL);
	ZZ tmp;
	ZZ_p hashv;
	ZZFromBytes(tmp,output,20);
	conv(hashv,tmp);

	/* get the hash table index out of it: */
	size_t bIndex;
	conv(bIndex,hashv);
	bIndex &= lmask;

	/* expand the prf key to get the key for
	 * the appropriate polyDel instance... */
	RNG_CTX ctx;
	unsigned char prfoutput[16];
	PRF(bIndex,prfoutput);
	init_RNG_CTX(ctx,prfoutput,16);
	polyDel<G> D;
	D.keyRestore(&ctx,maxDegree);

	zz_to_file(sockfd,hashv);
	/* server should respond with an integer (y) and a group element: */
	ZZ_p y;
	G t;
	zz_from_file(sockfd,y);
	t.from_file(sockfd);

	/* verify server's answer: */
	bool v = D.verify(hashv,y,t);

	if (y == 0) {
		if (terse) printf("1\t");
		else fprintf(stderr, "server says '%s' was found.\n",kw);
	}
	else {
		if (terse) printf("0\t");
		else fprintf(stderr, "server says '%s' was *NOT* found.\n",kw);
	}
	if (v) {
		if (terse) printf("1\n");
		else fprintf(stderr, "server's answer was verified.\n");
	}
	else {
		if (terse) printf("0\n");
		else fprintf(stderr, "server's answer was *NOT* verified.\n");
	}
	return 0;
}

int main(int argc, char *argv[])
{
	static int help=0;
	static int terse=0;
	static struct option long_opts[] = {
		{"port",       required_argument, 0, 'p'},
		{"host",       required_argument, 0, 'h'},
		{"init",       required_argument, 0, 'I'},
		{"search",     required_argument, 0, 's'},
		{"terse",      no_argument,       &terse,   1},
		{"help",       no_argument,       &help,    1},
		{0,0,0,0} // this denotes the end of our options.
	};
	int c; // holds an option
	int opt_index = 0;
	int port = 31337;
	char fnameIn[FNAME_MAX_LEN] = "input0";
	char hostname[FNAME_MAX_LEN] = "localhost";
	char word[KEYWORD_MAX_LEN] = "";
	/* ensure these are c-strings.  set null char, and don't
	 * copy more than len-1 chars */
	fnameIn[FNAME_MAX_LEN-1] = 0;
	hostname[FNAME_MAX_LEN-1] = 0;
	word[KEYWORD_MAX_LEN-1] = 0;
	bool doSearch = false;
	bool doInit = false;
	while ((c = getopt_long(argc, argv, "p:h:I:s:",
					long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'p':
				port = atoi(optarg);
				break;
			case 'h':
				strncpy(hostname,optarg,FNAME_MAX_LEN-1);
				break;
			case 'I':
				strncpy(fnameIn,optarg,FNAME_MAX_LEN-1);
				doInit = true;
				break;
			case 's':
				strncpy(word,optarg,KEYWORD_MAX_LEN-1);
				doSearch = true;
				break;
			case '?':
				return 1;
		}
	}

	if (help) {
		printf(usage,argv[0], port, hostname);
		return 0;
	}
	int rcode = 0;

	/* make sure we initialize the PRF: */
	algPRF<G>::init();

	/* at this point, we will always be using the network, so get it started: */
	initNetwork(hostname,port); // if this fails, then it also exits...

	if (doInit) {
		rcode = setup(fnameIn);
	}
	else if (doSearch) {
		rcode = search(word,terse);
	}

	shutdownNetwork();

	return rcode;
}

