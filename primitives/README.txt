Building
========

Run `make`.  This will build all available group libraries (currently just
integers and elliptic curves) into the library, and will build test programs
for both.  To build only the library and no test programs, use `make lib`.
The elliptic curve implementation depends on the PBC library; to build targets
only for the integer version, run them with `make EC=0`.

Notes
=====

Primitives are implemented as template classes, and are based on NTL for doing
polynomial arithmetic.  To use them with a new group, you just need to define
a simple interface for it.  You can see examples in `multgroup.h` and
`ecgroup.h`.  If you write a class which provides functions like those, it can
be plugged into the constructions for algebraic PRF's, polynomial delegation,
as well as the keyword search application, with essentially no changes to the
code.


Using NTL
=========

If you compile NTL with the configure parameters below, it will use GMP as a
back-end, and provide a more friendly interface, in addition to facility for
working with polynomials.  If you need to do some conversions between that and
GMP types, you can find code for it in `gmp-utils/gmp-ntl-conv.h`.


Building NTL
------------

You can get the source here:
http://www.shoup.net/ntl/ntl-6.0.0.tar.gz

To install, follow the instructions on the website.  For configure, I would
recommend something like this:

`./configure SHARED=on NTL_GMP_LIP=on CFLAGS="-march=native -O3" \
	LDFLAGS="-Wl,-O1"`

Note that you need a recent-ish version of GCC for -march=native.  If you have
a mac, you're probably out of luck.  Just take that flag out.

# vim:syntax=rst

