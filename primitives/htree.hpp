/* Hash tree template class. */
// #pragma once /* gcc bug?  you get a warning here... */
#ifndef __htree_hpp__
#define __htree_hpp__
/* TODO: see if #pragma once works again when you instantiate this.  Else,
 * I guess it is related to:
 * http://gcc.gnu.org/bugzilla/show_bug.cgi?id=47857  */

#include <stddef.h>
#include <vector>
using std::vector;
#include <utility>
using std::pair;
using std::make_pair;
#include <string.h> // memset.

#define DEBUG 1

#if DEBUG
#include <cstdio>
#endif

/* forward declarations */
template <typename T, typename H>
class hTree;

template <typename T, typename H>
struct htNode;

#if DEBUG
template <typename T, typename H>
void print(const htNode<T,H>* p, FILE* f) {
	if (!p) return;
	/* add a node entry for our node. */
	// fprintf(f,"  %i [shape=\"circle\", label=\"%i\"];\n",p->val,p->val);
	fprintf(f,"  %i [shape=\"circle\", label=\"%i\\n",p->val,p->val);
	for (size_t i = 0; i < 8; i++) {
		fprintf(f, "%02x", p->hash[i]);
	}
	fprintf(f, "\"];\n");
	if (p->left) {
		fprintf(f,"  %i -> %i [dir=\"forward\" arrowhead=\"none\"];\n",
				p->val,p->left->val);
		print(p->left,f);
	}
	if (p->right) {
		fprintf(f,"  %i -> %i [dir=\"forward\" arrowhead=\"none\"];\n",
				p->val,p->right->val);
		print(p->right,f);
	}
}

template <typename T, typename H>
void printPath(T& x, htNode<T,H>* root,
		vector<pair<htNode<T,H>*,htNode<T,H>*> >& path, const char* fname) {
	if (!fname) fname = "/tmp/testtree.dot";
	FILE* f = fopen(fname,"wb");
	fprintf(f, "digraph testtree {\n");
	fprintf(f, "  bgcolor=black\n  edge [color=white]\n");
	fprintf(f, "  node [style=filled color=white fillcolor=dodgerblue4 shape=circle]\n");

	htNode<T,H>* p = root;
	size_t j = 0;
	while (p && j<path.size()) {
		fprintf(f,"  %i [shape=\"circle\", label=\"%i\\n",p->val,p->val);
		for (size_t i = 0; i < 8; i++) {
			fprintf(f, "%02x", p->hash[i]);
		}
		fprintf(f, "\"];\n");

		if (path[j].first) {
			fprintf(f,"  %i -> %i [dir=\"forward\" arrowhead=\"none\"];\n",
					p->val,path[j].first->val);
		}
		if (path[j].second) {
			fprintf(f,"  %i -> %i [dir=\"forward\" arrowhead=\"none\"];\n",
					p->val,path[j].second->val);
		}
		if (x < p->val) p = path[j].first;
		else p = path[j].second;
		j++;
	}

	fprintf(f, "}\n");
	fclose(f);
}
#endif

template <typename T, typename H>
void clearPath(vector<pair<htNode<T,H>*,htNode<T,H>*> >& path) {
	for (size_t i = 0; i < path.size(); i++) {
		if (path[i].first) delete path[i].first;
		if (path[i].second) delete path[i].second;
	}
	path.clear();
}

/* for convenience: */
typedef unsigned char uchar;

/* NOTE: requirements for the datatype T:
 * must fit into a fixed number of bytes, and have a function
 * size_t toBytes(uchar*,size_t) that fills the entire buffer;
 * needs comparison operators ==, !=, <,>,<=,>=.
 * requirements for H
 * char* operator(char*,size_t,char* outBuf)
 * static size_t hLen;
 * bool check(uchar* hash, uchar* data, size_t len);
 * So, this will usually require writing some little wrappers. */

/** function for converting T to a byte array.
 * users must supply this for their T */
template <typename T>
size_t toBytes(const T& x, unsigned char* outBuf, size_t reqLen);

/** the maximum number of bytes for something of type T  */
#define MAX_DATA_LEN 128

/** Node class for hash tree. */
template <typename T, typename H>
struct htNode
{
	T val; ///< node's value
	uchar hash[H::hLen]; ///< hash of left+right children
	htNode<T,H>* left;
	htNode<T,H>* right;
	/** writes faithful representation (WHP) to a byte array.
	 * @param buf The buffer for output
	 * @return address of buffer (if null pointer was passed,
	 * this function allocates memory.) */
	uchar* toBytes(uchar* buf) {
		if (!buf) buf = new uchar[sLen];
		if (!this) {
			memset(buf,0,sLen);
		} else {
			::toBytes(this->val,buf,MAX_DATA_LEN);
			memcpy(buf+MAX_DATA_LEN,this->hash,H::hLen);
		}
		return buf;
	}
	/** utility function that takes a pair of nodes to the
	 * concatenated strings... */
	static uchar* pairToBytes(pair<htNode<T,H>*, htNode<T,H>*>& P, uchar* buf)
	{
		if (!buf) buf = new uchar[2*sLen];
		P.first->toBytes(buf);
		P.second->toBytes(buf+sLen);
		return buf;
	}
	/* TODO: wrap this with something that writes to a file descriptor.
	 * The wrapper can handle the situation for which pointers are 0 */

	/* recompute the hash based on the children.  This operation is
	 * not recursive. */
	int updateHash(uchar* buf) {
		/* NOTE: work buffer parameter is supplied for efficiency.
		 * you can overload this if you want a version that creates the
		 * buffer locally. */
		typename hTree<T,H>::HNPtrPair P(this->left,this->right);
		htNode<T,H>::pairToBytes(P,buf);
		hTree<T,H>::h(buf,2*sLen,this->hash);
		return 0;
	}

	htNode(T* const v = 0, htNode<T,H>* l = 0, htNode<T,H>* r = 0)
	{
		if (v) this->val = *v;
		this->left = l;
		this->right = r;
		uchar* buf = new uchar[2*sLen];
		this->left->toBytes(buf);
		this->right->toBytes(buf+sLen);
		hTree<T,H>::h(buf,2*sLen,this->hash);
	}

	/** copy constructor. */
	htNode(const htNode<T,H>& N)
	{
		this->val = N.val;
		memcpy(this->hash,N.hash,H::hLen);
		/* FIXME: I think this works for what you want, but
		 * might not be ideal.  certainly is not what one would expect. */
		this->left = 0;
		this->right = 0;
	}

#if DEBUG
	void dumpHash() {
		for (size_t i = 0; i < H::hLen; i++) {
			fprintf(stderr, "%02x", this->hash[i]);
		}
		fprintf(stderr, "\n");
	}
#endif

	static size_t sLen; ///< length when converted to a string.
};

template <typename T, typename H>
size_t htNode<T,H>::sLen = MAX_DATA_LEN + H::hLen;


/** This class implements a simple hash tree (Merkle tree) with
 * search functionality supporting verifiable positive and negative
 * results.  T can be pretty much anything for which the usual
 * comparison operators are defined; H needs to have an
 * operator()(uchar* in, size_t len, uchar* out).
 * Note also that every instance of T should take a fixed number
 * of bytes to avoid any ambiguity when feeding stuff into H.
 * */

template <typename T, typename H>
class hTree
{
friend class htNode<T,H>;
public:
	typedef pair<htNode<T,H>*, htNode<T,H>*> HNPtrPair;
	typedef pair<htNode<T,H>, htNode<T,H> > HNPair;
	hTree() {
		this->root = 0;
	}
	/** search -- return the search path for x  (run by server)
	 * @param[in] x the value to search for
	 * @param[out] path the search path
	 * @return true iff x was found.
	 * */
	bool search(const T& x, vector<HNPtrPair>& path)
	{
		htNode<T,H>* p = this->root;
		while (p && p->val != x) {
			HNPtrPair P;
			P.first = (p->left)?(new htNode<T,H>(*(p->left))):0;
			P.second = (p->right)?(new htNode<T,H>(*(p->right))):0;
			path.push_back(P);
			if (x < p->val) p = p->left;
			else p = p->right;
		}
		return p;
	}
	/** verify -- take a search path and a value x and verify it
	 * (run by client)
	 * @param[in] x the value which was searched for
	 * @param[in] path the search path
	 * @param[in] found the search result reported by the server.
	 * @return true iff all hashes are consistent and found is accurate
	 * */
	bool verify(const T& x, vector<HNPtrPair>& path, bool found)
	{
		/* NOTE: the vector does not have the root node, since
		 * both parties store that always. */
		htNode<T,H>* p = this->root;
		/* allocate storage for nodes as strings: */
		uchar* buf = new uchar[2*htNode<T,H>::sLen];
		size_t i = 0;
		while (i < path.size() && p) {
			htNode<T,H>::pairToBytes(path[i],buf);
			if (!h.check(p->hash,buf,2*htNode<T,H>::sLen)) {
				delete[] buf;
				return false;
			}
			if (x < p->val) p = path[i].first;
			else p = path[i].second;
			i++;
		}
		if (i != path.size()) return false; // p was null too soon.

		/* if we make it here, then the entire search path has been
		 * verified.  Now we need only to check correcteness of found. */
		if (!p) return !found;
		return (found && (p->val == x));
	}

	/** insert -- return is the same as search but also performs
	 * the insertion and updates the hashes (run by server)
	 * @param[in] x the value to insert
	 * @param[out] path the search path
	 * @return true iff insertion took place. */
	bool insert(T& x, vector<HNPtrPair>& path)
	{
		/* FIXME: this was supposed to be const T& x... */
		/* kind of annoying: you'll have to run through the tree
		 * twice to get this done as it is.  */
		vector<htNode<T,H>*> upath; // update path.
		htNode<T,H>* p = this->root;
		htNode<T,H>** pp = &this->root; // internal pointer to change
		while (p && p->val != x) {
			upath.push_back(p);
			HNPtrPair P;
			P.first = (p->left)?(new htNode<T,H>(*(p->left))):0;
			P.second = (p->right)?(new htNode<T,H>(*(p->right))):0;
			path.push_back(P);

			if (x < p->val) {
				pp = &p->left;
				p = p->left;
			} else {
				pp = &p->right;
				p = p->right;
			}
		}
		if (p) return false; // no insertion to be done.
		/* now insert node and fix up the hashes on the path. */
		*(pp) = new htNode<T,H>(&x);
		unsigned char* buf = new unsigned char[2*htNode<T,H>::sLen];
		for (int i = upath.size()-1; i >= 0; i--) {
			/* NOTE: these are actually internal node pointers in
			 * the tree, so you can use the left's and right's safely. */
			upath[i]->updateHash(buf);
		}
		delete[] buf;
		return true;
	}

	/** update -- takes the search path for x, and uses this to update
	 *            the root node (client).  The search path is first
	 *            verified before the root node is updated.
	 * @param[in] x the value which was to be inserted
	 * @param[in] path the search path returned by the server
	 * @param[in] modified the insertion status reported by the server.
	 * @param[in] erasePath clear / free the path parameter when done.
	 * @return true iff all hashes are consistent and modified is accurate
	 * */
	bool update(T& x, vector<HNPtrPair>& path,
				bool modified, bool erasePath=true)
	{
		/* if the search path doesn't check out, then exit with error. */
		if (!this->verify(x,path,!modified)) /* not modified means element found */
		{
			/* DEBUG: */
			//printPath(x,this->root,path,"/tmp/testtree2.dot");
			return false;
		}
		/* server's picture of the search path for x was correct; we can
		 * now update our root hash using the supplied path. */
		if (modified) {
			vector<htNode<T,H>*> upath; // update path.
			htNode<T,H>* p = this->root;
			htNode<T,H>** pp = &this->root; // internal pointer to change

			/* build list of nodes needing a new hash.  sigh. */
			for (size_t i = 0; i < path.size(); i++) {
				p->left = path[i].first;
				p->right = path[i].second;
				upath.push_back(p);
				if (x < p->val) {
					pp = &p->left;
					p = path[i].first;
				} else {
					pp = &p->right;
					p = path[i].second;
				}
			}
			/* you know p will be null, since that's the place where
			 * the new node is going (already checked the modified flag).
			 * So, we need to make a new node containing x and rebuild
			 * all the hashes on the path.  */
			*(pp) = new htNode<T,H>(&x);  /* FIXME: memory leak if not root */
			unsigned char* buf = new unsigned char[2*htNode<T,H>::sLen];
			for (int i = upath.size()-1; i >= 0; i--) {
				upath[i]->updateHash(buf);
			}

			delete[] buf;
			/* reset the left and right of the root to null: */
			this->root->left = this->root->right = 0;
		}

		if (erasePath)
			clearPath(path);

		return true;
	}

#if DEBUG
	/* NOTE: only works for integers. */

	template <typename X, typename Y>
	friend void print(const htNode<X,Y>* p, FILE* f);

	void drawTree(vector<HNPtrPair>* V = 0, const char* fname = 0) {
		if (!fname) fname = "/tmp/testtree.dot";
		FILE* f = fopen(fname,"wb");
		fprintf(f, "digraph testtree {\n");
		fprintf(f, "  bgcolor=black\n  edge [color=white]\n");
		fprintf(f, "  node [style=filled color=white fillcolor=dodgerblue4 shape=circle]\n");
		if (V) {
			/* NOTE: printing out this subgraph early can have the effect
			 * of reflecting some of the trees in the resulting graph from
			 * dot.  No big deal, but something to be aware of.  */
			fprintf(f, "  subgraph {\n");
			fprintf(f, "    node [style=filled fillcolor=palegreen3 shape=circle]\n");
			for (size_t i = 0; i < V->size(); i++) {
				if ((*V)[i].first)
					fprintf(f, "    %i ;\n",(*V)[i].first->val);
				if ((*V)[i].second)
					fprintf(f, "    %i ;\n",(*V)[i].second->val);
			}
			fprintf(f, "  }\n");
		}

		print(this->root, f);
		fprintf(f, "}\n");
		fclose(f);
	}
#endif

private:
	static H h; ///< hash function / MAC
	htNode<T,H>* root;
};


/* TODO:  make sure you have unique encodings for nodes when you plug
 * them into H.  Also document this.  Anyway, the point is that the
 * ultimate hash input, which is derived from the nodes L and R, must
 * be equal to another input if and only if L == L' && R == R' so that
 * the only way to create collisions is an attack on the hash function. */

/* FIXME: need construction / destruction everywhere */

#endif
