/* some utility functions for general groups. */
#pragma once

#include "gmp-utils.h"
#include <NTL/ZZ.h>
using namespace NTL;

/* for temporary storage of byte arrays. */
static unsigned char tmpBytes[MP_MAX_BYTES];

/* implementation of Brickell-like algorithm */
template <typename G>
int exp_fixedbase(G& r, const G& g, const ZZ& y, G** sxtPow)
{
	/* compute max # of base 16 digits: */
	size_t nlimbs = NumBytes(y);
	size_t nibblesPerDigit = sizeof(tmpBytes[0])*2;
	size_t npows = ((nlimbs+1) * nibblesPerDigit);
	size_t i;
	BytesFromZZ(tmpBytes,y,nlimbs);
	ZZ sxtn;
	conv(sxtn,16);
	G* spow = *sxtPow; // convenient alias for 16th powers array
	if (*sxtPow == 0) {
		/* for now, not going to optimize the precomputation part much,
		 * since it is not supposed to happen all that often. */
		*sxtPow = new G[npows];
		spow = *sxtPow;
		spow[0] = G::getGenerator();
		for (i = 1; i < npows; i++) {
			G::exp(spow[i],spow[i-1],sxtn);
			/* NOTE: have to be careful to make sure the above
			 * call is not recursive.  It shouldn't be, since spow[0]
			 * ought to be a *copy* of the generator, but still... */
		}
		/* NOTE: also have to make sure that the first time this is
		 * called, the exponent y has as many bytes as it will ever have. */
	}
	/* NOTE: for groups of integers, this would go a lot faster using
	 * Montgomery operations... */
	G b; b.setToIdent();
	r.setToIdent();
	unsigned char yd;
	unsigned char mask16 = 0xF;
	for (size_t j=15; j>0; j--) {
		i=0; // i will now index nibbles
		for (size_t k=0; k<nlimbs; k++) {
			yd = tmpBytes[k];
			for (size_t t=0; t<nibblesPerDigit; t++) {
				if ((yd & mask16) == j) {
					G::mul(b,b,spow[i]);
				}
				yd >>= 4;
				i++;
			}
		}
		G::mul(r,r,b);
	}

	return (int)npows;
}

template <typename G>
int exp_fixedbase(G& r, const G& g, const ZZ_p& y, G** sxtPow)
{
	return exp_fixedbase(r,g,rep(y),sxtPow);
}
