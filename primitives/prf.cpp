/* pseudorandom function based on AES.
 * */

#include <cstdio>
#include <string.h>
#include <openssl/aes.h>
#include <inttypes.h>
#include <openssl/sha.h>
#include "prf.h"

static unsigned char KEY_BYTES[16]; /* bytes of key for the prf */
static AES_KEY key; /* openssl formatted key. */
static uint64_t IV_CTR[2]; /* [0] is the IV; [1] the counter */
/* random stuff needed by openssl: */
static unsigned char ecount[16];
static unsigned int num;

int PRFSetSeed(void* buffer, size_t bufLen)
{
	/* set up misc. variables needed by openssl's implementation. */
	bzero(ecount,16);
	num = 0;
	/* first hash the buffer: */
	unsigned char seed[32];
	SHA256((unsigned char*)buffer,bufLen,seed);
	memcpy(KEY_BYTES,seed,16);
	memcpy(IV_CTR,seed,8);
	AES_set_encrypt_key(KEY_BYTES,128,&key);
	return 0;
}

int init_RNG_CTX(RNG_CTX& ctx, void* buffer, size_t bufLen)
{
	/* set up misc. variables needed by openssl's implementation. */
	bzero(ctx.ecnt,16);
	ctx.num = 0;
	num = 0;
	unsigned char seed[32];
	if (!buffer) {
		/* use /dev/urandom for now. */
		FILE* frand = fopen("/dev/urandom", "rb");
		fread(seed,1,32,frand);
		fclose(frand);
	} else {
		SHA256((unsigned char*)buffer,bufLen,seed);
	}
	memcpy(ctx.keyBytes,seed,16);
	memcpy(ctx.counter,seed,16);
	/* above call initializes the IV, but also sets the counter
	 * to a random initial location. */
	AES_set_encrypt_key(ctx.keyBytes,128,&ctx.key);
	return 0;
}

int randBytes(unsigned char* outBuf, size_t len, RNG_CTX& ctx)
{
	size_t nBlocks = len / 16;
	/* encrypt 0's with current counter. */
	uint64_t input[2];
	input[0] = input[1] = 0;
	for (size_t i = 0; i < nBlocks; i++) {
		AES_ctr128_encrypt((unsigned char*)input, outBuf, 16, &ctx.key,
				(unsigned char*)ctx.counter, ctx.ecnt, &ctx.num);
		outBuf += 16;
	}
	if (len % 16) {
		/* need one more invocation. */
		unsigned char tmpBuf[16];
		AES_ctr128_encrypt((unsigned char*)input, tmpBuf, 16, &ctx.key,
				(unsigned char*)ctx.counter, ctx.ecnt, &ctx.num);
		for (size_t i = 0; i < len % 16; i++) {
			outBuf[i] = tmpBuf[i];
		}
	}
	return 0;
}

int PRF(uint64_t i, void* rv)
{
	IV_CTR[1] = i;
	uint64_t input[2];
	input[0] = input[1] = 0;
	AES_ctr128_encrypt((unsigned char*)input, (unsigned char*)rv,
			16, &key, (unsigned char*)IV_CTR, ecount, &num);
	return 0;
}
