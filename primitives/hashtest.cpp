/* simple test program for hash tree. */
#include <cstdio>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "htree.hpp"

#include <openssl/sha.h>
#include <vector>
using std::vector;

/* Template construction requires a hash function and data. */

/* TODO: move some of this to a new file */
class sha256 {
public:

	unsigned char* operator()(unsigned char* inBuf,
							  size_t bufLen,
							  unsigned char* outBuf)
	{
		SHA256(inBuf,bufLen,outBuf);
		return outBuf;
	}

	bool check(unsigned char* hash, unsigned char* data, size_t len) {
		unsigned char h2[hLen];
		SHA256(data,len,h2);
		for (size_t i = 0; i < hLen; i++) {
			if (hash[i] != h2[i]) {
#if DEBUG
				fprintf(stderr, "Failed hash input (%lu bytes):\n",len);
				for (size_t i = 0; i < len; i++) {
					fprintf(stderr, "%02x", data[i]);
				}
				fprintf(stderr, "\nHash was:\n");
				for (size_t i = 0; i < hLen; i++) {
					fprintf(stderr, "%02x", h2[i]);
				}
				fprintf(stderr, "\nShould have been:\n");
				for (size_t i = 0; i < hLen; i++) {
					fprintf(stderr, "%02x", hash[i]);
				}
				fprintf(stderr, "\n~~~~~~~~~~~~~~~~~~~\n");
#endif
				return false;
			}
		}
		return true;
	}

	static const size_t hLen = 32;

private:
	/* If you change this to a MAC, then you'd want the
	 * seed to be stored here... */
};

/* this function is required by hTree. */
template <typename T>
size_t toBytes(const T& x, unsigned char* outBuf, size_t reqLen)
{
	memcpy(outBuf,&x,sizeof(T));
	memset(outBuf + sizeof(T),0,reqLen - sizeof(T));
	return sizeof(T);
}
/* NOTE: you might be able to get away with a non-template version of this
 * if you define it before you #include htree.hpp.  The problem with this
 * is that you lose the ability to overload it in a non-trivially different
 * manner. */

uint64_t clockGetTime_mu() {
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000000LL + (uint64_t)ts.tv_nsec / 1000LL;
}

int twoTreeTest(size_t n=15)
{
	/* no network stuff, but try out the basic operations where
	 * one tree stores only the root. */

	hTree<int,sha256> stree; /* server */
	hTree<int,sha256> ctree; /* client */

	vector<hTree<int,sha256>::HNPtrPair> V; /* shared... */
	// randomly generate tree.
	int k;
	vector<int> items; /* save what we insert for later searching. */
	bool modified, verified, found;
	size_t i;
	for (i = 0; i < n; i++) {
		k = rand();
		items.push_back(k);
		modified = stree.insert(k,V);
		verified = ctree.update(k,V,modified,true);
		if (!verified)
			fprintf(stderr, "Update %lu failed x_x\n",i);
	}
	modified = stree.insert(k,V);
	// verified = ctree.update(k,V,modified,false);
	verified = ctree.update(k,V,modified,true);
	if (!verified)
		fprintf(stderr, "Update %lu failed x_x\n",i);
	stree.drawTree();

	/* now test the search functions: */

	for (size_t i = 0; i < n; i++) {
		int r = rand() % items.size();
		found = stree.search(items[r],V);
		verified = ctree.verify(items[r],V,found);
		if (!found)
			fprintf(stderr, "%i not found (line %d)\n",items[r],__LINE__);
		if (!verified) {
			fprintf(stderr, "answer for %i not verified (line %d)\n",
					items[r],__LINE__);
		}
		clearPath(V);
		/* now search for something that probably isn't there. */
		r = rand();
		found = stree.search(r,V);
		verified = ctree.verify(r,V,found);
		if (found)
			fprintf(stderr, "random elt (%i) found (line %d)\n",r,__LINE__);
		if (!verified)
			fprintf(stderr, "answer for %i not verified (line %d)\n",
					r,__LINE__);
		clearPath(V);
	}
	return 0;
}

int simpleTest(size_t n=15)
{
	hTree<int,sha256> tree;
	vector<hTree<int,sha256>::HNPtrPair> V;
	int k = 23;
	// bool modified = tree.insert(k,V);
	// tree.update(k,V,modified);

	// randomly generate tree.
	for (size_t i = 0; i < n; i++) {
		k = rand();
		tree.insert(k,V);
	}

	/* NOTE: after each insert call, V will have pointers
	 * to a bunch of copies of the nodes, which the caller
	 * will be responsible for cleaning up. */
	clearPath(V);
	k = 99;
	tree.insert(k,V);
	tree.drawTree(&V);
	return 0;
}

/* for timing tests, a few that isolate a single function: */
int insertTest(size_t n=15)
{
	hTree<int,sha256> tree;
	vector<hTree<int,sha256>::HNPtrPair> V;

	for (size_t i = 0; i < n; i++) {
		int r = rand(); /* param 1 to insert is T&, so rvalue not allowed */
		tree.insert(r,V);
	}

	/* NOTE: Experiments show that as a percentage of what's going on, very
	 * little time is wasted in system calls.  Delete's implementation must
	 * contribute to sys time, right?  So it seems like having Lots of little
	 * deletions isn't a big deal here. */
	clearPath(V);
	return 0;
}

int searchTest(size_t n=15)
{
	uint64_t tStart, tStop, tTotal=0;
	/* first the inserts: */
	hTree<int,sha256> tree;
	vector<hTree<int,sha256>::HNPtrPair> V;

	for (size_t i = 0; i < n; i++) {
		int r = rand(); /* param 1 to insert is T&, so rvalue not allowed */
		tree.insert(r,V);
	}
	clearPath(V);

	srand(time(NULL));
	/* now time the search calls.  We'll take an average. */
	size_t nTrials = 50;
	size_t lenTotal = 0;
	for (size_t i = 0; i < nTrials; i++) {
		tStart = clockGetTime_mu();
		int r = rand();
		bool found = tree.search(r,V);
		lenTotal += V.size();
		/* now run the verification.  For this, note that we can
		 * run verify on the same object, since it won't modify
		 * the data.  Which reminds me: verify() should be const. */
		tree.verify(r,V,found);
		tStop = clockGetTime_mu();
		tTotal += (tStop - tStart);
		clearPath(V);
	}
	printf("%lu\t%f\t%f\n", n,
			(double)tTotal / nTrials,
			(double)lenTotal / nTrials);

	return 0;
}

typedef int (*testfn)(size_t); /* test function prototype */
testfn tests[] = {&twoTreeTest, &simpleTest, &insertTest, &searchTest};

int main(int argc, char *argv[]) {
	size_t t = (argc>1)?(atol(argv[1]) % (sizeof(tests)/sizeof(tests[0]))):0;
	size_t n = (argc>2)?(atol(argv[2])):23;
	return tests[t](n);
}
