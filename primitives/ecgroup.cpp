/*Implementation of an elliptic group based on PBC library*/
#include "ecgroup.h"
#include "garithmetic.hpp"

/* static member initialization. */
uint32_t pBytes[] = {
#include "pBytesEC.txt"
};

/* Give all the statics a home: */
const mpz_class ecgroup::p=mpz_from_dwords(pBytes,sizeof(pBytes)/sizeof(pBytes[0]));
const field_ptr ecgroup::Fp = (field_ptr)pbc_malloc(sizeof(*ecgroup::Fp));
element_t ecgroup::a;
element_t ecgroup::b;
ecgroup* ecgroup::sxtPow = 0;
mpz_t ecgroup::ord;
const field_ptr ecgroup::fieldCurve = (field_ptr)pbc_malloc(sizeof(*ecgroup::Fp));

/* and now actually initialize them: */
bool ecgroup::initSuccess = doSetup();
ecgroup ecgroup::gen; // now set to the identity... fix it:
bool ecgroup::initGenSuccess = doGenSetup();

bool ecgroup::doSetup()
{
	/* setup the field parameters */
	mpz_t ptemp;
	mpz_init(ptemp);
	mpz_set(ptemp,p.get_mpz_t());
#ifdef USE_NIST
	field_init_nist192_fp(Fp);
	//field_init_naive_fp(Fp,ptemp);
	//field_init_fp(Fp,ptemp);
#else
	field_init_fp(Fp,ptemp);
#endif
	element_init(a,Fp);
	element_init(b,Fp);
	mpz_t aZ,bZ; mpz_init(aZ); mpz_init(bZ);
	mpz_set_str(aZ, "0xfffffffffffffffffffffffffffffffefffffffffffffffc", 0);
	mpz_set_str(bZ, "0x64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1", 0);
	element_set_mpz(a,aZ);
	element_set_mpz(b,bZ);
	mpz_init(ord);
	mpz_set_str(ord, "0xffffffffffffffffffffffff99def836146bc9b1b4d22831", 0);
	field_init_curve_ab(fieldCurve, a, b, ord, 0);
	mpz_clear(ptemp);mpz_clear(aZ);mpz_clear(bZ);
	return true;
}

bool ecgroup::doGenSetup()
{
	/* setup the generator: */
	element_t gen_e;
	element_init(gen_e,fieldCurve);
	point_ptr P = (point_ptr)pbc_malloc(sizeof(*P));
	element_init(P->x, Fp);
	element_init(P->y, Fp);
	mpz_t xZ,yZ; mpz_init(xZ); mpz_init(yZ);
	mpz_set_str(xZ, "0x188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012", 0);
	mpz_set_str(yZ, "0x07192b95ffc8da78631011ed6b24cdd573f977a11e794811", 0);
	element_set_mpz(P->x,xZ);
	element_set_mpz(P->y,yZ);
	P->inf_flag = 0;
	gen_e->data=P;
	element_set(ecgroup::gen.elt,gen_e); /* does a deep copy. */
	/* DEBUG */
	#if 0
	element_t t1,t2;
	element_init(t1,Fp); element_init(t2,Fp);
	element_set(t1,P->x); element_set1(t2);
	for (size_t i = 1; i < 4; i++) {
		element_mul(t2,t2,t1);
		element_printf("t2**%lu\t= %B\n",i,t2);
	}
	#endif
	mpz_clear(xZ);mpz_clear(yZ);
	element_clear(gen_e);
	/* END DEBUG */
	return true;
}

/* group element no-arg constructor should set elements to the identity.
   Might need to set this constructor or another to construct field and somehow
   guarantee that it's called only once. */
ecgroup::ecgroup() {
	element_init(this->elt,this->fieldCurve);
	element_set0(this->elt);	//sets element to the identity
}	//no-arg constructor ends

ecgroup::ecgroup(const ecgroup& b) {
	element_init(this->elt,this->fieldCurve);
	element_set(elt, *((element_t*)(&b.elt)));
}

ecgroup::~ecgroup() {
	element_clear(this->elt);
}

int ecgroup::mul(ecgroup& r, const ecgroup& a, const ecgroup& b) {
	element_add(r.elt, *(element_t*)&a.elt, *(element_t*)&b.elt);	
	return 0;
}

int ecgroup::exp(ecgroup& r, const ecgroup& a, const ZZ& b)	{	
	/* if a is actually the generator, then use the fixed base
	 * methods instead: */
	if (&a == &ecgroup::gen)
		return exp_fixedbase(r,a,b,&ecgroup::sxtPow);
	/* element_mul_mpz should call element_pow_mpz, which should
	 * compute Z-module action as desired. */
	mpz_class c;
	zztompz(c,b);
	element_mul_mpz(r.elt, *(element_t*)&a.elt, c.get_mpz_t());
	return 0;
}

int ecgroup::exp(ecgroup& r, const ecgroup& a, const ZZ_p& b) {
	return ecgroup::exp(r,a,rep(b));
}

ZZ ecgroup::order()	{
	ZZ o;
	mpztozz(o, mpz_class(ord));
	return o;
}

const ecgroup& ecgroup::getGenerator() {
	return ecgroup::gen;
}

void ecgroup::operator*=(const ecgroup& b) {
	element_add(this->elt, *(element_t*)&b.elt, this->elt);
	return;
}

void ecgroup::operator=(const ecgroup& b) {
	if (this->elt==*(element_t*)&b.elt) return;
	element_set(this->elt, *((element_t*)(&b.elt)));
}

	/*	Returns true if this->elt == b.elt
	@param ecgroup 		instance
	@return bool 		this->elt==b.elt
	*/
bool ecgroup::operator==(const ecgroup& b) {
	return this->equals(b);
}

bool ecgroup::equals(const ecgroup& b) {
	/* element_cmp does not work.  have to compare the points manually. */
	point_ptr Pl = (point_ptr)this->elt->data;
	point_ptr Pr = (point_ptr)b.elt->data;
	mpz_t l,r; mpz_init(l); mpz_init(r);
	element_to_mpz(l,Pl->x);
	element_to_mpz(r,Pr->x);
	if (mpz_cmp(l,r)) return false;
	element_to_mpz(l,Pl->y);
	element_to_mpz(r,Pr->y);
	if (mpz_cmp(l,r)) return false;
	mpz_clear(l); mpz_clear(r);
	return true;
}

void ecgroup::setToIdent() {
	element_set0(this->elt);
}

void ecgroup::clear() {
	/* TODO: find a good way to zero mpz variables. */
	element_clear(this->elt);
	return;
}

void ecgroup::to_file(int fd) {
	/* grab the coords (x,y), get the mpz_t values, and write
	 * those to the file descriptor. */
	point_ptr P = (point_ptr)this->elt->data;
	write(fd,&(P->inf_flag),sizeof(P->inf_flag));
	if (P->inf_flag) return; /* no need to store the rest */
	mpz_t xZ,yZ; mpz_init(xZ); mpz_init(yZ);
	element_to_mpz(xZ,P->x);
	element_to_mpz(yZ,P->y);
	mpz_to_file(fd,mpz_class(xZ));
	mpz_to_file(fd,mpz_class(yZ));
	mpz_clear(xZ); mpz_clear(yZ);
}

void ecgroup::from_file(int fd) {
	point_ptr P = (point_ptr)this->elt->data;
	read(fd,&(P->inf_flag),sizeof(P->inf_flag));
	if (P->inf_flag) return;
	mpz_class xZ,yZ;
	mpz_from_file(fd,xZ);
	mpz_from_file(fd,yZ);
	element_set_mpz(P->x,xZ.get_mpz_t());
	element_set_mpz(P->y,yZ.get_mpz_t());
}

void ecgroup::print() {
	point_ptr P = (point_ptr)this->elt->data;
	if (P->inf_flag) {
		printf("(O,O)\n");
		return;
	}
	mpz_t xZ,yZ; mpz_init(xZ); mpz_init(yZ);
	element_to_mpz(xZ,P->x);
	element_to_mpz(yZ,P->y);
	gmp_printf("(%Zx,%Zx)\n",xZ,yZ);
	mpz_clear(xZ); mpz_clear(yZ);
}

void ecgroup::selfTest() {
	printf("Field of definition is prime? -- %i\n", ISPRIME(ecgroup::p));
	printf("Order of group is prime? -- %i\n",
			mpz_probab_prime_p(ecgroup::ord,10));
	ecgroup S,T;
	point_ptr PS = (point_ptr)S.elt->data;
	point_ptr PT = (point_ptr)T.elt->data;
	mpz_t xs,ys; mpz_init(xs); mpz_init(ys);
	mpz_set_str(xs,"0xd458e7d127ae671b0c330266d246769353a012073e97acf8",0);
	mpz_set_str(ys,"0x325930500d851f336bddc050cf7fb11b5673a1645086df3b",0);
	element_set_mpz(PS->x,xs);
	element_set_mpz(PS->y,ys);
	mpz_t xt,yt; mpz_init(xt); mpz_init(yt);
	mpz_set_str(xt,"0xf22c4395213e9ebe67ddecdd87fdbd01be16fb059b9753a4",0);
	mpz_set_str(yt,"0x264424096af2b3597796db48f8dfb41fa9cecc97691a9c79",0);
	element_set_mpz(PT->x,xt);
	element_set_mpz(PT->y,yt);
	PS->inf_flag = PT->inf_flag = 0;
	ecgroup R;
	printf("Nist sample point S:\n\t");
	S.print();
	printf("Nist sample point T:\n\t");
	T.print();
	printf("S + T:\n\t");
	ecgroup::mul(R,S,T);
	R.print();
	printf("2S:\n\t");
	ecgroup::mul(R,S,S);
	R.print();
	mpz_clear(xs); mpz_clear(ys);mpz_clear(xt); mpz_clear(yt);
}
