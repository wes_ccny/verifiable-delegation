/* interface for multiplicative group of integers modulo a prime. */
#pragma once

#include <NTL/ZZ_p.h>
using namespace NTL;
#include <gmpxx.h>

/* * * * * * * * tuning * * * * * * * */
#ifndef USE_MGM
#define USE_MGM 1
#endif
/* Use montgomery multiplication, rather than generic
 * multiplication when doing fixed base exponentiation.
 * This should provide a modest speed improvement. (10%?) */
/* * * * * * * * * * * * * * * * * * */

/** Store / manipulate elements of the multiplicative group of integers modulo
 * a prime q.  The parameters (prime q, generator g, ord(g)=p) are fixed,
 * static members. */
class multGrp
{
public:
	multGrp();
	multGrp(const multGrp&);
	~multGrp();

	/** exposes the group operation */
	static int mul(multGrp& r, const multGrp& a, const multGrp& b);
	/** efficient computation of Z-module action. */
	static int exp(multGrp& r, const multGrp& a, const ZZ& b);
	static int exp(multGrp& r, const multGrp& a, const ZZ_p& b);
	/** returns the order of the group */
	static ZZ order();
	/** sets g to be a generator of the group */
	static const multGrp& getGenerator();

	void operator*=(const multGrp& b);
	void operator=(const multGrp& b);
	bool operator==(const multGrp& b);
	// I think it is safer to reference an explict function, since
	// there will be a default implementation of ==, which might not
	// be what you want.
	bool equals(const multGrp& b); ///< test for equality
	void setToIdent(); ///< set (*this) to identity
	void clear(); ///< free memory, set (*this) to a fixed value
	void to_file(int fd); ///< writes to a file descriptor.
	void from_file(int fd); ///< read from a file descriptor.

	/* for debugging: */
	void print(); ///< human-readable output for debugging, etc.
	static void selfTest();

// private:
	/* global group parameters: */
	/* FIXME: make all these const again. */
	static mpz_class q; ///< prime modulus
	static mpz_class g; ///< generator of subgroup of Z_q*
	static mpz_class p; ///< ord(g) in Z_q*
#if USE_MGM
	static mpz_t* sxtPow;   ///< storage for fixed base exp.
#else
	static multGrp* sxtPow;   ///< storage for fixed base exp.
#endif
	/* montgomery reduction parameters: */
	static mpz_t R;
	static mp_limb_t qprime;


	mpz_class elt; ///< the element of <g> represented by (*this)

	static multGrp gen; ///< generator of group (wraps mpz_class g)
	static bool initSuccess;
	static bool doSetup();
};
