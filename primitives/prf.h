/** pseudorandom function based on AES.
 * */
#pragma once

#include <openssl/aes.h>
#include <inttypes.h>

/** Initialize the PRF.
 * NOTE: The input buffer must contain high-entropy data to avoid
 * brute force attacks.
 * NOTE: bufLen is measured in bytes. */
int PRFSetSeed(void* buffer, size_t bufLen);

/** input is the integer i; output is 16 pseudorandom bytes, stored in rv.
 * Return value is 0 if the call succeeded.
 * NOTE: rv must point to a buffer of at least 16 bytes. */
int PRF(uint64_t i, void* rv);

/** random number generator context. */
struct RNG_CTX {
	unsigned char ecnt[16];
	unsigned char keyBytes[16];
	uint64_t counter[2]; /* [1] is the counter. */
	unsigned int num;
	AES_KEY key;
};

/** Initializes random number generator context.
 * If supplied, the buffer and bufLen parameters will deterministically
 * Initialize the context using the buffer as an entropy source;
 * if left null, system randomness will be used to create the context. */
int init_RNG_CTX(RNG_CTX& ctx, void* buffer=0, size_t bufLen=0);

/** local-state random number generator.
 * generates len pseudorandom bytes based on the given context */
int randBytes(unsigned char* outBuf, size_t len, RNG_CTX& ctx);
