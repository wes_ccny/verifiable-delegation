/* interface for elliptic group of integers modulo a prime. */
#pragma once

#define PBC_DEBUG
#ifdef USE_NIST
#include <pbc.h>
extern "C" {
#include <pbc_fp.h>
}
#else
#include <pbc/pbc.h>
extern "C" {
#include <pbc/pbc_fp.h>
}
#endif
#include "gmp-utils.h"
#include <NTL/ZZ_p.h>
using namespace NTL;
#include <gmpxx.h>
#include "gmp-ntl-conv.h"
#include "unistd.h"
#include <string>
#include <cstring>
#include <string.h>
#include <sstream>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <iostream>

	//Per-element data. Elements of this group are points on the elliptic curve.
typedef struct {
  int inf_flag;    // inf_flag == 1 means O, the point at infinity.
  element_t x, y;  // Otherwise we have the finite point (x, y).
} *point_ptr;

class ecgroup	{
public:
	ecgroup();
	ecgroup(const ecgroup&);
	~ecgroup();

	static int mul(ecgroup& r, const ecgroup& a, const ecgroup& b);

	/** compute Z-module action (scalar mult)  */
	static int exp(ecgroup& r, const ecgroup& a, const ZZ& b);	
	static int exp(ecgroup& r, const ecgroup& a, const ZZ_p& b);	

	/** returns the order of the group */
	static ZZ order();								

	/** sets g to be a generator of the group */
	static const ecgroup& getGenerator();

	void operator*=(const ecgroup& b);
	void operator=(const ecgroup& b);
	bool operator==(const ecgroup& b);
	bool equals(const ecgroup& b); ///< test for equality
	void setToIdent(); ///< set (*this) to identity
	void clear(); ///< free memory, set (*this) to a fixed value

	void to_file(int fd); ///< writes to a file descriptor.
	void from_file(int fd); ///< read from a file descriptor.
	void print(); ///< human-readable output for debugging, etc.
	static void selfTest();

	static const field_ptr fieldCurve;

	static element_t a;
	static element_t b;
	static mpz_t ord; /* order of the group */
	/* for faster fixed base multiplication: */
	static ecgroup* sxtPow;
	element_t elt;

	static const mpz_class p; ///< field of definition for curve.
	static ecgroup gen; ///< the generator

private:
	static const field_ptr Fp;
	static bool initSuccess;
	static bool doSetup();
	static bool initGenSuccess;
	static bool doGenSetup();
};
