#include <cstdio>
#include <iostream>
using std::cout;
using std::endl;
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>

#ifdef USE_EC
#include "ecgroup.h"
typedef ecgroup G; // your elliptic curve type goes here...
#else
#include "gmp-utils.h" /* prevent GMP_USE_CXX11 from being turned on */
#include "multgroup.h"
typedef multGrp G;
#endif

#include "alg-prf.hpp"
#include "poly-del.hpp"

#include "garithmetic.hpp"
#include "gmp-ntl-conv.h"

#include "prf.h"

/* should give a microsecond timer. */
uint64_t clockGetTime_mu() {
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000000LL + (uint64_t)ts.tv_nsec / 1000LL;
}

/* read the time stamp counter */
extern "C" {
  __inline__ uint64_t rdtsc(void) {
    uint32_t lo, hi;
    __asm__ __volatile__ (      // serialize
    "xorl %%eax,%%eax \n        cpuid"
    ::: "%rax", "%rbx", "%rcx", "%rdx");
	/* We cannot use "=A", since this would use %rax on x86_64 and return only
	 * the lower 32bits of the TSC */
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return (uint64_t)hi << 32 | lo;
  }
}

int testPolyDel()
{
	// invent a polynomial to outsource:
	ZZ_pX f;
	f.SetMaxLength(32);
	cout << "coeffs = { ";
	/* NOTE: I think you need to allocate enough space for f
	 * in general.  By default, NTL will not check the length
	 * inside of operator[] */
	for (size_t i = 1; i < 10; i++) {
		SetCoeff(f,i-1,i*i*i);
		cout << f.rep[i-1] << " ";
	}
	cout << "}" << endl;

	polyDel<G> D(f);
	D.keyGen();
	// invent a place to evaluate the polynomial:  <-- x in [0,d]?
	ZZ_p x;
	x = 2202394270;
	polyDel<G> server;
	D.split(server);
	ZZ_p y;
	G t;
	server.compute(x,y,t);
	cout << "\nf(" << x << ") = " << y << endl;
	cout << "Verification: " << D.verify(x,y,t) << endl;
	return 0;
}

/* Report the time for initial delegation and for a query
 * on a random polynomial of degree d */
int testPolyDelTiming(size_t d, bool useTSC=false, bool terse=true)
{
	ZZ_pX f;
	ZZ_p r,x;
	/* create random polynomial */
	f.SetMaxLength(d+1);
	for (size_t i = 0; i <= d; i++) {
		random(r);
		SetCoeff(f,i,r);
		//cout << f.rep[i-1] << " ";
	}
	polyDel<G> D(f);

	/* get a random evaluation point */
	random(x);

	uint64_t tStart, tStop;
	/* time keygen */
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	D.keyGen();

	if (useTSC) {
		tStop = rdtsc();
		if (!terse) cout << "TSCs for delegation phase: ";
	} else {
		tStop = clockGetTime_mu();
		if (!terse) cout << "microseconds for delegation phase: ";
	}
	cout << (tStop - tStart) << endl;

	/* now time a query response */
	polyDel<G> server;
	D.split(server);
	G t;
	ZZ_p y;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	server.compute(x,y,t);

	if (useTSC) {
		tStop = rdtsc();
		if (!terse) cout << "TSCs for query response: ";
	} else {
		tStop = clockGetTime_mu();
		if (!terse) cout << "microseconds for query response: ";
	}
	cout << (tStop - tStart) << endl;

	if (!terse) cout << "\nf(" << x << ") = " << y << endl;
	if (!terse) cout << "Verification: " << D.verify(x,y,t) << endl;
	return 0;
}

int testAlgPRF()
{
	algPRF<G> F;
	F.keyGen();
	for (size_t i = 0; i < 5; i++) {
		gmp_printf("F(%lu)\t= ",i);
		F(i).print();
	}

	ZZ_p x;
	unsigned long d = 10;
	for (size_t i = 1718233; i < 1718235; i++) {
		x = i;
		printf("   CFE(%lu,%lu) =",i,d);
		F.CFE(x,d).print();
		printf("nonCFE(%lu,%lu) =",i,d);
		F.nonCFE(x,d).print();
	}
	return 0;
}

int testStash()
{
	// generate a polyDel object, write to a file,
	// read from that file, and see if anything changed.
	ZZ_pX f;
	f.SetMaxLength(32);
	cout << "coeffs = { ";
	for (size_t i = 1; i < 10; i++) {
		SetCoeff(f,i-1,i*i*i);
		cout << f.rep[i-1] << " ";
	}
	cout << "}" << endl;

	polyDel<G> D(f);
	D.keyGen();

	// now the parameters are set; write it.
	cout << "D.F.k0 = " << D.F.k0 << endl;
	cout << "D.F.k1 = " << D.F.k1 << endl;
	D.stash("teststash");
	polyDel<G> E;
	E.unstash("teststash");
	cout << "E.F.k0 = " << E.F.k0 << endl;
	cout << "E.F.k1 = " << E.F.k1 << endl;
	// well, it reads one value properly.  check the rest without printing.
	/* first check the sizes */
	if (deg(D.P) != deg(E.P))
		fprintf(stderr, "Degrees are %li,%li respectively.\n",
				deg(D.P),deg(E.P));
	if (D.T.size() != E.T.size())
		fprintf(stderr, "Tag sizes are %lu,%lu respectively.\n",
				D.T.size(),E.T.size());
	for (long i = 0; i < D.P.rep.length(); i++) {
		if (D.P.rep[i] != E.P.rep[i])
			fprintf(stderr, "coeffs differ at %li\n",i);
	}
	for (size_t i = 0; i < D.T.size(); i++) {
		if (! D.T[i].equals(E.T[i]))
			fprintf(stderr, "tag coeffs differ at %lu\n",i);
	}
	return 0;
}

int testGroup()
{
	/* run some basic tests of the group operation. */
	/* copy constructor works? */
	G g1;
	g1 = G::getGenerator();
	g1.print();
	G g2(g1);
	g2.print();
	/* test the exponentiation / order. */
	ZZ o = G::order();
	G g3;
	G::exp(g3,g2,o);
	g3.print();
	/* default to identity? */
	G g0;
	g0.print();
	return 0;
}

int testTiming(unsigned long nTrialsMul, unsigned long nTrialsExp, bool useTSC)
{
	/* report some numbers for the time it takes to multiply
	 * group elements, and to exponentiate the generator. */
	G x,y,r;
	/* NOTE: it is important here to *alias* the generator,
	 * rather than making a copy of it.  This will make sure
	 * that the more efficient fixed-base exponentiation is used. */
	G& g = G::gen;
	/* to get some random elements, we'll just take
	 * g to random powers. */
	ZZ_p e;
	random(e); G::exp(x,g,e);
	random(e); G::exp(y,g,e);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (unsigned long i = 0; i < nTrialsMul; i++) {
		/* multiply group elements */
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
		G::mul(r,x,y);
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs for group operation: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds for group operation: ";
	}
	cout << (double)(tStop - tStart) / (8*nTrialsMul) << endl;

	/* now test exponentiation: */
	random(e);
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (unsigned long i = 0; i < nTrialsExp; i++) {
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
		G::exp(r,g,e);
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs for exponentiation: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds for exponentiation: ";
	}
	cout << (double)(tStop - tStart) / (8*nTrialsExp) << endl;
	return 0;
}

int testFBExp(unsigned long nTrials, bool useTSC)
{
	G g,r;
	g = G::getGenerator();
	ZZ e;
	RandomBnd(e,G::order());
	G::exp(r,g,e);
	cout << "plain computation:\t"; r.print();
	G* pow16 = 0;
	/* first call will setup the powers: */
	int npows = exp_fixedbase(r,g,e,&pow16);
	cout << "fixed base computation:\t"; r.print();
	fprintf(stderr, "Computed table of %i values.\n",npows);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();
	for (unsigned long i = 0; i < nTrials; i++) {
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
		exp_fixedbase(r,g,e,&pow16);
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs for exponentiation: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds for exponentiation: ";
	}
	cout << (double)(tStop - tStart) / (8*nTrials) << endl;
	delete[] pow16;
	return 0;
}

int testPRF()
{
	char prfkey[] = "inappropriatetestkey";
	PRFSetSeed(prfkey,strlen(prfkey));

	/* now print some pseudo-random values 8D */
	printf("Testing PRF...\n");
	uint64_t temp[2];
	for (size_t i = 0; i < 10; i++) {
		PRF(i,temp);
		temp[0] ^= temp[1];
		fprintf(stderr, "prf(%lu) = %lu\n",i,temp[0]);
	}

	/* Now test the local-state RNG stuff: */
	RNG_CTX ctx;
	init_RNG_CTX(ctx, prfkey, strlen(prfkey));
	uint64_t temp2[8];
	randBytes((unsigned char*)temp2,32,ctx);
	randBytes((unsigned char*)(temp2+4),32,ctx);
	printf("RNG output:\n");
	for (size_t i = 0; i < 8; i++) {
		printf("%lu\n",temp2[i]);
	}
	/* one more time, with #bytes != 0 mod 16 */
	randBytes((unsigned char*)temp2,23,ctx);
	unsigned char* t2bytes = reinterpret_cast<unsigned char*>(temp2);
	for (size_t i = 0; i < 23; i++) {
		printf("%u\n",t2bytes[i]);
	}
	return 0;
}

int main(void)
{
	/* NOTE: you must call setModulus() before declaring any
	 * algPRF variables.  This is an artifact of the way NTL
	 * deals with the ZZ_p class. */
	algPRF<G>::init();
	//return testFBExp(20,false);
	//return testPolyDelTiming(1024);
	for (size_t i = 1000; i <= 10000; i+=1000) {
		printf("degree %lu:\n",i);
		testPolyDelTiming(i);
	}
	return 0;
	return testPRF();
	return testTiming(1000,20,false);
	G::selfTest();
	return testAlgPRF();
	return testGroup();
	return testStash();
	return testPolyDel();
}
