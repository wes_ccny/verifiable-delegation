/* gmp-utils.cpp
 * Implementation of gmp-utils.h
 * */

#include "gmp-utils.h"
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#define Z_MAX_BYTES 512
unsigned char zBufGlobal[Z_MAX_BYTES]; // for temporary storage.
mpz_t Ared;
/* above is used by montgomeryRed; placed here to prevent repeated
 * allocation / deallocation */

/* Like read(), but retry on EINTR and EWOULDBLOCK,
 * abort on other errors, and don't return early. */
void xread(int fd, void *buf, size_t nBytes)
{
	do {
		ssize_t n = read(fd, buf, nBytes);
		if (n < 0 && errno == EINTR) continue;
		if (n < 0 && errno == EWOULDBLOCK) continue;
		if (n < 0) perror("read"), abort();
		buf = (char *)buf + n;
		nBytes -= n;
	} while (nBytes);
}

/* Like write(), but retry on EINTR and EWOULDBLOCK,
 * abort on other errors, and don't return early. */
void xwrite(int fd, const void *buf, size_t nBytes)
{
	do {
		ssize_t n = write(fd, buf, nBytes);
		if (n < 0 && errno == EINTR) continue;
		if (n < 0 && errno == EWOULDBLOCK) continue;
		if (n < 0) perror("write"), abort();
		buf = (const char *)buf + n;
		nBytes -= n;
	} while (nBytes);
}


// TODO: this method of generating random integers is not sufficient
// for crypto stuff.
gmp_randclass R(gmp_randinit_default);

/* NOTE: modular multiplication will require two function calls, I think.
 * for a single operation, the two calls are fine.  If you have occasion to do
 * a bunch in a row, then use montgomery stuff is better.
 * */

/* compute a*b mod n; store result in r */
void MULMOD(mpz_class& r, const mpz_class& a, const mpz_class& b, const mpz_class& n)
{
	MUL(r,a,b);
	MOD(r,r,n);
}

bool montgomeryInit(mpz_ptr R, mp_limb_t* mprime, mpz_srcptr m, mpz_ptr A)
{
	/* set up the parameters for montgomery reduction.
	 * R = b^k, where k is minimal such that R > m
	 * mprime = -m^-1 mod base */
	mpz_t nModb; mpz_init(nModb);
	mpz_t a; mpz_init(a);
	mpz_t b; mpz_init(b);
	/* set b to be the base for our integers: */
	mpz_set_str(b,"0x10000000000000000",0); /* base 2**64 */
	/* initialize nModb to be m mod b: */
	mpz_mod(nModb,m,b);
	/* this would also work, but since this init function is called
	 * O(1) times per process, who cares?  */
	// mpz_set_ui(nModb,m->_mp_d[0]);

	/* find a modular inverse: */
	mpz_invert(a,nModb,b);
	/* now compute additive inverse mod base */
	mpz_sub(a,b,a);

	*mprime = a->_mp_d[0];
	/* ok, now set R to the right value, which is R =  b^k where k is the
	 * number of digits of m */

	mpz_set_ui(R,1);
	mpz_mul_2exp(R,R,(sizeof(mp_limb_t)*8*m->_mp_size));

	/* finally, set A to have enough storage for the strange usage
	 * in montgomeryRed below. */
	/* FIXME: this doesn't work for some reason... */
	//mpz_init2(A,2*sizeof(mp_limb_t)*8*(m->_mp_size + 2));
	mpz_init2(Ared,2*sizeof(mp_limb_t)*8*(m->_mp_size + 2));
	mpz_clear(a);
	mpz_clear(b);
	mpz_clear(nModb);
	return true;
}

void montgomeryRed(mpz_ptr a, mpz_ptr T, mpz_ptr m, mp_limb_t mprime)
{
	if (a->_mp_size < m->_mp_size)
		mpz_realloc2(a,sizeof(mp_limb_t)*8*(1+m->_mp_size));
#if USE_ASM_RED
	mp_limb_t cy;
	cy = mpn_redc_1(a->_mp_d, T->_mp_d, m->_mp_d, m->_mp_size, mprime);
	if (cy || mpn_cmp(a->_mp_d,m->_mp_d,m->_mp_size) >= 0) {
		mpn_sub_n(a->_mp_d,a->_mp_d,m->_mp_d,m->_mp_size);
	}
	a->_mp_size = m->_mp_size;
	/* FIXME: I think the following is needed for correctness
	 * in general.  On random data, this code will work with
	 * high probability, but not everywhere.  However, this
	 * application works only with (a subgroup of) the multiplicative
	 * group, so you at least will not run into the issue of an
	 * incorrect representation of 0. */
	#if 0
	size_t newLen = m->_mp_size;
	while (!a->_mp_d[--newLen] && newLen != (size_t)-1);
	a->_mp_size = ++newLen;
	if (mpz_cmp(a,m) >= 0)
		mpz_sub(a,a,m);
	#endif
	/* FIXME: USE_ASM_RED seems to give correct results in isolation,
	 * and on random data, but when used with exponentiation, causes
	 * heap corruptions...  :\ */
#else

	size_t i;
	size_t mLen = m->_mp_size;
	/* now copy T.digits, and put in high order zeros. Important:  T is assumed
	 * to be less than nR so that it will have length less than 2*n.len */
	mpz_set(Ared,T);
	mp_limb_t* A = Ared->_mp_d;
	/* ensure high-order stuff is zeroed */
	CLEARHIGH(Ared);

	mp_limb_t u;
	mp_limb_t res = 0;
	for(i=0; i<mLen; i++)
	{
		u = mprime*A[i]; //this will wrap modulo the base.
		res = mpn_addmul_1(A+i,m->_mp_d,mLen,u);
		/* now take the leftover carry, and apply it to the rest of A: */
		res = mpn_add_1(A+i+mLen,A+i+mLen,mLen-i+1,res);
	}

	/* copy the digits back into a and set the length, etc. manually. */
	size_t newLen = m->_mp_size+1;
	mpn_copyi(a->_mp_d, A+mLen, newLen);
	while (!a->_mp_d[--newLen]);
	a->_mp_size = ++newLen;

	if (mpz_cmp(a,m) >= 0)
		mpz_sub(a,a,m);

#endif
}

/* TODO: allow one to override the temporary storage Ared and
 * supply their own value */
void montgomeryMul(mpz_ptr a, mpz_ptr x, mpz_ptr y, mpz_ptr m, mp_limb_t mprime)
{
	/* does montgomery multiplication:  computes a = xyR^-1 mod m */
	/* pad x with zeros to avoid branching below. */
	size_t mLen = m->_mp_size;
	mpz_realloc2(x,sizeof(mp_limb_t)*8*mLen);
	CLEARHIGH(x);
	size_t i;
	mp_limb_t u;
	/* IDEA: allocate a large digit buffer to begin with.  The many divisions
	 * by the base (which would naturally be taken care of with right shifts)
	 * can then be handled in *constant* time simply by incrementing a pointer.
	 * Just be careful to preserve the mpz_t invariants. */

	/* First, allocate a long representation of 0. The high order bits will
	 * eventually become the return value.*/
	/* we can reuse Ared?  It has enough size. */
	mp_limb_t* A = Ared->_mp_d;
	mpn_zero(A,Ared->_mp_alloc);
	/* preserve invariants: */
	Ared->_mp_size = 0; /* set Ared to 0 */

	mp_limb_t res;
	for(i=0; i<mLen; i++)
	{
		u = ((A[0] + x->_mp_d[i]*y->_mp_d[0])*mprime);
		res = mpn_addmul_1(A,y->_mp_d,y->_mp_size,x->_mp_d[i]);
		A[y->_mp_size] += res;
		res = mpn_addmul_1(A,m->_mp_d,mLen,u);
		res = mpn_add_1(A+mLen,A+mLen,mLen-i+1,res);
		A++; /* "right shift" by incrementing the pointer */
	}
	/* copy back into a: */
	mpz_realloc2(a,sizeof(mp_limb_t)*8*(mLen + 3));
	size_t newLen = mLen+3;
	mpn_copyi(a->_mp_d, A, newLen);
	while (!a->_mp_d[--newLen]);
	a->_mp_size = ++newLen;

	if (mpz_cmp(a,m) >= 0)
		mpz_sub(a,a,m);
	return;
}

int powm_fixedbase(mpz_ptr r, mpz_ptr g, mpz_ptr y, mpz_ptr n,
		mpz_ptr R, mp_limb_t nprime, mpz_t** sxtPow)
{
	/* compute max # of base 16 digits: */
	size_t nlimbs = y->_mp_size;
	size_t nibblesPerDigit = sizeof(mp_limb_t) * 2;
	size_t npows = ((nlimbs+1) * nibblesPerDigit);
	size_t i;
	mpz_t* spow = *sxtPow; // convenient alias for 16th powers array
	if (*sxtPow == 0) {
		/* for now, not going to optimize the precomputation part much,
		 * since it is not supposed to happen all that often. */
		*sxtPow = (mpz_t*) malloc(npows * sizeof(mpz_t));
		spow = *sxtPow;
		mpz_init(spow[0]);
		//mpz_set(spow[0],g);
		mpz_mul(spow[0],g,R);
		mpz_mod(spow[0],spow[0],n);
		for (i = 1; i < npows; i++) {
			mpz_init(spow[i]);
			montgomeryMul(spow[i],spow[i-1],spow[i-1],n,nprime);
			montgomeryMul(spow[i],spow[i],spow[i],n,nprime);
			montgomeryMul(spow[i],spow[i],spow[i],n,nprime);
			montgomeryMul(spow[i],spow[i],spow[i],n,nprime);
		}
	}
	mpz_t b; mpz_init(b);
	/* set b,r to 1*R mod n, instead of just 1: */
	mpz_tdiv_r(b,R,n);
	mpz_set(r,b);
	mp_limb_t yd;
	mp_limb_t mask16 = 0xF;
	for (size_t j=15; j>0; j--) {
		i=0; // i will now index nibbles
		for (size_t k=0; k<nlimbs; k++) {
			yd = y->_mp_d[k];
			for (size_t t=0; t<nibblesPerDigit; t++) {
				if ((yd & mask16) == j) {
#if MGM_MIXED
					mpz_mul(b,b,spow[i]);
					montgomeryRed(b,b,n,nprime);
#else
					montgomeryMul(b,b,spow[i],n,nprime);
#endif
				}
				yd >>= 4;
				i++;
			}
		}
#if MGM_MIXED
		mpz_mul(r,r,b);
		montgomeryRed(r,r,n,nprime);
#else
		montgomeryMul(r,r,b,n,nprime);
#endif
	}
	montgomeryRed(r,r,n,nprime);

	return (int)npows;
}
void randomPrime(mpz_class& p, unsigned long nBits)
{
	do {
		p = R.get_z_bits(nBits);
	} while (!ISPRIME(p));
	return;
}

void genSafePrime(mpz_class& p, unsigned long nBits)
{
	mpz_class p1; // store (p-1) / 2
	while (true) {
		randomPrime(p,nBits);
		// NOTE: since p1 is odd, p >> 1 == (p-1) / 2
		p1 = p >> 1;
		if (ISPRIME(p1))
			return;
	}
}

void mpz_from_bytes(mpz_class& r, const unsigned char* inBuf, size_t l)
{
	mpz_import(r.get_mpz_t(),l,-1,1,0,0,inBuf);
}

void bytes_from_mpz(unsigned char*& outBuf, size_t* l, const mpz_class& n)
{
	outBuf = (unsigned char*)mpz_export(outBuf,l,-1,1,0,0,n.get_mpz_t());
}

mpz_class mpz_from_dwords(uint32_t* inBuf, size_t nWords, int endian)
{
	mpz_t tmp;
	mpz_init(tmp);
	mpz_import(tmp,nWords,endian,4,0,0,inBuf);
	mpz_class rv = mpz_class(tmp);
	mpz_clear(tmp);
	return rv;
}

/* use bytes_from_mpz to write n to a file in the following format:
 * 8 bytes for the length of n, followed by the bytes of n, produced
 * by the above function bytes_from_mpz */
void mpz_to_file(int fd, const mpz_class& n)
{
	unsigned char* zBuf = zBufGlobal;
	size_t zLen = 0;
	bytes_from_mpz(zBuf,&zLen,n);
	xwrite(fd,&zLen,sizeof(size_t));
	xwrite(fd,zBuf,zLen);
}

/* reads an integer from an open file descriptor.
 * uses the same format as mpz_to_file */
void mpz_from_file(int fd, mpz_class& n)
{
	unsigned char* zBuf = zBufGlobal;
	size_t zLen = 0;
	xread(fd,&zLen,sizeof(size_t));
	xread(fd,zBuf,zLen);
	mpz_from_bytes(n,zBuf,zLen);
}
