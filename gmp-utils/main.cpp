#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::cin;
using std::endl;
#include "gmp-utils.h"

#include <time.h>

// should give me a microsecond timer.
uint64_t clockGetTime_mu() {
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000000LL + (uint64_t)ts.tv_nsec / 1000LL;
}

// readtsc function, stolen from wikipedia:
// http://en.wikipedia.org/wiki/Time_Stamp_Counter
extern "C" {
  __inline__ uint64_t rdtsc(void) {
    uint32_t lo, hi;
    __asm__ __volatile__ (      // serialize
    "xorl %%eax,%%eax \n        cpuid"
    ::: "%rax", "%rbx", "%rcx", "%rdx");
	/* We cannot use "=A", since this would use %rax on x86_64 and return only
	 * the lower 32bits of the TSC */
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return (uint64_t)hi << 32 | lo;
  }
}

int testMulTiming(unsigned long nTrials, unsigned long nBits, bool useTSC)
{
	/* Here are some basic tests of the C++ interface to gmp.
	 * We'll test how long multiplication takes.  It seems that for a
	 * newer sandybridge, you can multiply 2048 bit integers, and only spend
	 * 2.4 clock cycles per 64 bit limb O_O  (Still slower than the theoretical
	 * throughput, but faster than the latency (3 cc))
	 * */
	mpz_class a,b,c,d;
	//a = R.get_z_bits(nBits);
	b = R.get_z_bits(nBits);
	c = R.get_z_bits(nBits);
	d = R.get_z_bits(nBits);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (unsigned long i = 0; i < nTrials; i++) {
		// POWERMOD(a,b,c,d);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (8*nTrials) << endl;
	return 0;
}

int mulmodTest()
{
	/* read 3 integers, a,b,c from standard input, and print the
	 * modular multiplication a*b mod c to stdout.
	 * */
	mpz_class a,b,c,d;
	string s;
	while(true) {
		cin >> s;
		a = s.c_str();
		cin >> s;
		b = s.c_str();
		cin >> s;
		c = s.c_str();
		MULMOD(d,a,b,c);
		cout << d << endl;
	}
	return 0;
}

int powermodTest()
{
	/* read 3 integers, a,b,c from standard input, and print the
	 * modular exponentiation a^b mod c to stdout.
	 * */
	mpz_class a,b,c,d;
	string s;
	while(true) {
		cin >> s;
		a = s.c_str();
		cin >> s;
		b = s.c_str();
		cin >> s;
		c = s.c_str();
		POWERMOD(d,a,b,c);
		cout << d << endl;
	}
	return 0;
}

int primeTest()
{
	/* generating some random prime numbers.  Read the
	 * number of bits from stdin, and then print a
	 * prime of that size to stdout.
	 * */
	uint16_t nBits;
	mpz_class p;
	while (cin >> nBits) {
		randomPrime(p,nBits);
		cout << p << endl;
	}
	return 0;
}

int safePrimeTest()
{
	uint16_t nBits;
	mpz_class p;
	while (cin >> nBits) {
		genSafePrime(p,nBits);
		cout << p << endl;
	}
	return 0;
}

int conversionTest()
{
	unsigned char A[] = {1,2,3,4,5};
	// this should be 1 + 2*256 + 3*256**2 + 4*256**3 + 5*256**4 = 21542142465
	mpz_class n;
	mpz_from_bytes(n,A,5);
	cout << n << endl;
	// and now try this in reverse:
	unsigned char B[5];
	unsigned char* C = B;
	size_t l;
	bytes_from_mpz(C,&l,n);
	for (size_t i = 0; i < 5; i++) {
		cout << (unsigned int)B[i] << "  ";
	}
	// make sure B == C
	cout << endl << reinterpret_cast<unsigned long*>(B) << " "
		<< reinterpret_cast<unsigned long*>(C) << endl;
	// NOTE: the interface for bytes_from_mpz is a little clunky if you have
	// a statically allocated array, but it will work fine otherwise, which
	// will usually(?) be the case.  Here's an example:
	unsigned char* D = 0;
	bytes_from_mpz(D,&l,n);
	for (size_t i = 0; i < 5; i++) {
		cout << (unsigned int)D[i] << "  ";
	}
	cout << endl;
	delete[] D;
	return 0;
}

int powmodFixedTest()
{
	size_t bits = 160;
	mpz_t r,m,Ared;
	mpz_init(r); mpz_init(m);
	mp_limb_t mprime;
	mpz_class mm;
	randomPrime(mm,bits);
	mpz_set(m,mm.get_mpz_t());
	montgomeryInit(r,&mprime,m,Ared);

	mpz_t r2,a,b;
	mpz_t* pow16 = 0;
	mpz_init(r2); mpz_init(a); mpz_init(b);
	gmp_scanf("%Zd %Zd",a,b);
	int npows = powm_fixedbase(r2,a,b,m,r,mprime,&pow16);
	/* return value is the size of the allocated array
	 * of 16th powers. */

	for (int i=0; i<npows; i++)
		mpz_clear(pow16[i]);
	free(pow16);
	fprintf(stderr, "allocated %i values.\n",npows);
	gmp_fprintf(stderr, "Modulus was: %Zd\n",m);
	gmp_fprintf(stderr, "Answer was: %Zd\n",r2);
	mpz_powm(r2,a,b,m);
	gmp_fprintf(stderr, "Regular version: %Zd\n",r2);
	return 0;
}

/* For speed, we will save p192 globally, as well as
 * a little storage for intermediate calculations. */
static mpz_t randprod;
static mpz_t p192;
static mp_limb_t p192limbs[4]; /* for mpn comparisons. */

int p192setup()
{
	mpz_init(p192);
	mpz_set_str(p192, "0xfffffffffffffffffffffffffffffffeffffffffffffffff", 0);
	//mpz_init2(t,256); mpz_init2(s1,256); mpz_init2(s2,256); mpz_init2(s3,256);
	mpn_copyi(p192limbs,p192->_mp_d,3);
	p192limbs[3] = 0;

	mpz_class y;
	y = R.get_z_bits(384);
	mpz_init(randprod);
	mpz_set(randprod,y.get_mpz_t());
	return 0;
}

/* modular reduction for 6-digit (base 2**64) integer
 * modulo NIST's p192 */
static void p192_mod(mpz_ptr r, mpz_srcptr a) {
  //PBC_ASSERT(a->_mp_size < 7, "reducing value > p192**2");
  /* new approach: allocate a few arrays of limbs, only use
   * mpn calls, and nevermind the sizes until the end. */
  mp_limb_t t[4];
  mp_limb_t s1[3];
  mp_limb_t s2[3];
  mp_limb_t s3[3];

  t[0] = a->_mp_d[0];
  t[1] = a->_mp_d[1];
  t[2] = a->_mp_d[2];

  s1[0] = a->_mp_d[3];
  s1[1] = a->_mp_d[3];
  s1[2] = 0;

  s2[0] = 0;
  s2[1] = a->_mp_d[4];
  s2[2] = a->_mp_d[4];

  s3[0] = a->_mp_d[5];
  s3[1] = a->_mp_d[5];
  s3[2] = a->_mp_d[5];

  t[3] = mpn_add_n(t,t,s1,3);
  t[3] += mpn_add_n(t,t,s2,3);
  t[3] += mpn_add_n(t,t,s3,3);

  while (mpn_cmp(t,p192limbs,4) >= 0)
	mpn_sub_n(t,t,p192limbs,4);
  /* t has the right stuff. */
  /* to avoid re-allocating, we'll require r has at least 3 limbs. */
  /* could call mpz_import(r,3,-1,sizeof(mp_limb_t),0,0,t); but it is
   * way too slow.  Instead, copy values and set the size manually */

  r->_mp_d[0] = t[0];
  r->_mp_d[1] = t[1];
  r->_mp_d[2] = t[2];
#if 1
  r->_mp_size = 3;
  while (r->_mp_size && r->_mp_d[r->_mp_size-1] == 0)
	  --r->_mp_size;
#else
  /* for comparison: */
  size_t rlen;
  __asm__ ("xor %0, %0\n"
		  "xor %%rax, %%rax\n"
		  "sub %1, %%rax\n"
		  "adc $0, %0\n"
		  "xor %%rax, %%rax\n"
		  "sub %2, %%rax\n"
		  "adc $0, %0\n"
		  "xor %%rax, %%rax\n"
		  "sub %3, %%rax\n"
		  "adc $0, %0\n"
		  :"=r"(rlen)
		  :"r"(t[0]),"r"(t[1]),"r"(t[2])
		  :"%rax");

  r->_mp_size = rlen;
  if (rlen != 3) {
	  while (r->_mp_size && r->_mp_d[r->_mp_size-1] == 0)
		  --r->_mp_size;
  }
#endif

  return;
}

int p192redTest(unsigned long nTrials, bool usep192)
{
	/* see if we can make a modular reduction for p192 which
	 * is substantially faster than the generic reduction. */
	mpz_t r; mpz_init(r);

	uint64_t tStart=0, tStop=0;
	uint64_t tMin = (uint64_t)-1;

	/* new strategy: see you get more reliable results by taking
	 * a lot of small measurements.  I think this is what the
	 * gmp speed programs do. */
	if (usep192) {
		for (unsigned long i = 0; i < nTrials; i++) {
			tStart = rdtsc();
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			p192_mod(r,randprod);
			tStop = rdtsc();
			if (tStop - tStart < tMin)
				tMin = tStop - tStart;
		}
	} else {
		for (unsigned long i = 0; i < nTrials; i++) {
			tStart = rdtsc();
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			mpz_mod(r,randprod,p192);
			tStop = rdtsc();
			if (tStop - tStart < tMin)
				tMin = tStop - tStart;
		}
	}
	cout << "TSCs: ";
	cout << (double)(tStop - tStart) / 8 << endl;
	return 0;
}

int powmodTiming(unsigned long nTrials, unsigned long nBits,
		bool useTSC, bool useFB)
{
	mpz_t r,Ared; mpz_init(r);
	mp_limb_t mprime;
	mpz_class m;
	randomPrime(m,nBits);
	montgomeryInit(r,&mprime,m.get_mpz_t(),Ared);

	mpz_class a,g,y;
	g = R.get_z_bits(nBits);
	y = R.get_z_bits(nBits);
	MOD(y,y,m);
	MOD(g,g,m);
	mpz_t* table = 0;

	/* don't measure the precomputation: */
	int npows = POWERMOD_FB(a,g,y,m,r,mprime,&table);

	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	if (useFB) {
		for (unsigned long i = 0; i < nTrials; i++) {
			POWERMOD_FB(a,g,y,m,r,mprime,&table);
		}
	} else {
		for (unsigned long i = 0; i < nTrials; i++) {
			POWERMOD(a,g,y,m);
		}
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (nTrials) << endl;

	// clear out the table.
	for (int i=0; i<npows; i++)
		mpz_clear(table[i]);
	free(table);
	if (useFB) {
		/* correctness check: */
		mpz_class acheck;
		POWERMOD(acheck,g,y,m);
		if (a != acheck) {
			gmp_fprintf(stderr, "FB result:\t%Zx\n",a.get_mpz_t());
			gmp_fprintf(stderr, "Plain result:\t%Zx\n",acheck.get_mpz_t());
			fprintf(stderr, "Other params:\n");
			gmp_fprintf(stderr, "\tg:\t%Zx\n",g.get_mpz_t());
			gmp_fprintf(stderr, "\ty:\t%Zx\n",y.get_mpz_t());
			gmp_fprintf(stderr, "\tm:\t%Zx\n",m.get_mpz_t());
		} else {
			fprintf(stderr, "Check passed.\n");
		}
	}
	return 0;
}

int mredTiming(size_t nTrials, size_t nBits, bool useTSC)
{
	/* test out montgomery reduction. */
	mpz_t r,m,Ared;
	mpz_init(r); mpz_init(m);
	mp_limb_t mprime;
	mpz_class mm;
	randomPrime(mm,nBits);
	mpz_set(m,mm.get_mpz_t());
	montgomeryInit(r,&mprime,m,Ared);
	/* select a random value x which is about twice as large as m,
	 * but must be less than m*r */
	mpz_class T;
	mpz_t mr; mpz_init(mr);
	mpz_mul(mr,m,r);
	mpz_t a; mpz_init2(a,sizeof(mp_limb_t)*8*(m->_mp_size + 4));
	//mpz_t a; mpz_init(a);

	T = R.get_z_bits(2*nBits);
	mpz_mod(T.get_mpz_t(),T.get_mpz_t(),mr);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (size_t i = 0; i < nTrials; i++) {
#if 0
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
		montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
#else
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
		montgomeryRed(a,T.get_mpz_t(),m,mprime);
#endif
	}

	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (nTrials) << endl;
	return 0;
}

int mredTest(size_t nTrials, size_t nBits)
{
	/* test out montgomery reduction. */
	mpz_t r,m,Ared;
	mpz_init(r); mpz_init(m);
	mp_limb_t mprime;
	mpz_class mm;
	randomPrime(mm,nBits);
	mpz_set(m,mm.get_mpz_t());
	montgomeryInit(r,&mprime,m,Ared);
	/* select a random value x which is about twice as large as m,
	 * but must be less than m*r */
	mpz_class T;
	mpz_t mr; mpz_init(mr);
	mpz_mul(mr,m,r);
	mpz_t a; mpz_init(a);
	mpz_class acheck;
	mpz_class tcheck;
	mpz_class rinv;
	size_t failCount = 0;
	bool srcEqDest = false;
	for (size_t i = 0; i < nTrials; i++) {
		T = R.get_z_bits(2*nBits);
		mpz_mod(T.get_mpz_t(),T.get_mpz_t(),mr);
		tcheck = T; /* save a copy to test in-place stuff */

		/* reduction will compute TR\inv mod m */
		if (srcEqDest) {
			montgomeryRed(T.get_mpz_t(),T.get_mpz_t(),m,mprime);
			mpz_set(a,T.get_mpz_t());
		}
		else
			montgomeryRed(a,T.get_mpz_t(),m,mprime);
		/* and now compute it naively: */
		mpz_set(rinv.get_mpz_t(),r);
		INVERT(rinv,rinv,mm);
		MULMOD(acheck,tcheck,rinv,mm);

		if (mpz_cmp(a,acheck.get_mpz_t())) {
			failCount++;
			gmp_fprintf(stderr, "Montg result:\t%.*Zx\n",nBits/4,a);
			gmp_fprintf(stderr, "Plain result:\t%.*Zx\n",nBits/4,
					acheck.get_mpz_t());
			fprintf(stderr, "Other params:\n");
			gmp_fprintf(stderr, "\tT:\t%.*Zx\n",nBits/4,T.get_mpz_t());
			gmp_fprintf(stderr, "\tm:\t%.*Zx\n",nBits/4,m);
		}
	}
	if (!failCount)
		fprintf(stderr, "Check passed.\n");
	else
		fprintf(stderr, "%lu failures out of %lu tests.\n",failCount,nTrials);
	return 0;
}

int plainredTiming(size_t nTrials, size_t nBits, bool useTSC)
{
	mpz_class m;
	/* do the montgomery setup stuff for authenticity... */
	mpz_class r;
	mpz_t Ared;
	mp_limb_t mprime;
	randomPrime(m,nBits);
	montgomeryInit(r.get_mpz_t(),&mprime,m.get_mpz_t(),Ared);
	/* select a random value x which is about twice as large as m,
	 * but must be less than m*r */
	mpz_class T;
	mpz_class mr;
	MUL(mr,m,r);
	mpz_class a;

	T = R.get_z_bits(2*nBits);
	MOD(T,T,mr);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (size_t i = 0; i < nTrials; i++) {
#if 0
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
		MOD(T,T,m);
#else
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
		MOD(a,T,m);
#endif
	}

	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (nTrials) << endl;
	return 0;
}

int mmulTest(size_t nTrials)
{
	/* test out montgomery multiplication. */
	size_t bits = 256;
	mpz_t r,m,Ared;
	mpz_init(r); mpz_init(m);
	mp_limb_t mprime;
	mpz_class mm;
	randomPrime(mm,bits);
	mpz_set(m,mm.get_mpz_t());
	montgomeryInit(r,&mprime,m,Ared);
	mpz_class xx,yy;
	mpz_t x,y,a;
	mpz_init(x); mpz_init(y); mpz_init(a);

	size_t failCount = 0;
	size_t firstWrong = 0;
	for (size_t i = 0; i < nTrials; i++) {
		xx = R.get_z_bits(bits);
		MOD(xx,xx,mm);
		yy = R.get_z_bits(bits);
		MOD(yy,yy,mm);
		mpz_set(x,xx.get_mpz_t());
		mpz_set(y,yy.get_mpz_t());
#define SRC_EQ_DEST 0

#if SRC_EQ_DEST == 1
		montgomeryMul(x,x,y,m,mprime);
#elif SRC_EQ_DEST == 2
		montgomeryMul(x,x,x,m,mprime);
#else
		montgomeryMul(a,x,y,m,mprime);
#endif
		/* now we have xyR\inv; mult with R^2? */
		mpz_t rr; mpz_init(rr);
		mpz_mul(rr,r,r);
		mpz_mod(rr,rr,m);
		mpz_t b; mpz_init(b);
		mpz_class bcheck;
#if SRC_EQ_DEST == 1
		montgomeryMul(b,rr,x,m,mprime);
		MULMOD(bcheck,xx,yy,mm);
#elif SRC_EQ_DEST == 2
		montgomeryMul(b,rr,x,m,mprime);
		MULMOD(bcheck,xx,xx,mm);
#else
		montgomeryMul(b,rr,a,m,mprime);
		MULMOD(bcheck,xx,yy,mm);
#endif
		if (mpz_cmp(b,bcheck.get_mpz_t())) {
			if (!(failCount++)) firstWrong = i;
			gmp_fprintf(stderr, "Montg result:\t%Zx\n",b);
			gmp_fprintf(stderr, "Plain result:\t%Zx\n",bcheck.get_mpz_t());
			fprintf(stderr, "Other params:\n");
			gmp_fprintf(stderr, "\tx:\t%Zx\n",x);
			gmp_fprintf(stderr, "\ty:\t%Zx\n",y);
			gmp_fprintf(stderr, "\tm:\t%Zx\n",m);
		}
	}
	if (!failCount) {
		fprintf(stderr, "Check passed.\n");
		gmp_fprintf(stderr, "Here's m:\t%Zx\n",m);
	}
	else {
		fprintf(stderr, "%lu/%lu tests passed.\n",nTrials-failCount,nTrials);
		fprintf(stderr, "First wrong was %lu.\n",firstWrong);
	}
	return 0;
}

int main(int argc, const char *argv[])
{
	/* First, setup our random numbers... */
	// TODO: for an actual application, use /dev/random
	FILE* frand = fopen("/dev/urandom","rb");
	unsigned char seed[32];
	fread(seed,1,32,frand);
	fclose(frand);
	mpz_t S;
	mpz_init(S); // is this necessary?  Not sure.
	mpz_import(S,32,-1,1,0,0,seed);
	mpz_class SC(S);
	R.seed(SC);
	mpz_clear(S);

	bool useTSC = true;
	unsigned long nTrials = 100000;
	unsigned long nBits = 2048;
	#if 0
	cout << "GMP info:\n"
		<< "cflags:\t\t" << __GMP_CFLAGS << endl
		<< "compiler:\t\t" << __GMP_CC << endl
		<< "bits/limb\t\t" << mp_bits_per_limb << endl
		<< "gmp ver.\t\t" << gmp_version << "\n\n";
	#endif

	int whichTest = 0;
	if (argc > 1)
		whichTest = atoi(argv[1]);

	switch (whichTest) {
		case 0:
			printf("Running prime test...\n");
			return primeTest();
		case 1:
			printf("Running powermod test...\n");
			return powermodTest();
		case 2:
			printf("Running mulmod test...\n");
			return mulmodTest();
		case 3:
			printf("Running multiplication timing test...\n");
			return testMulTiming(nTrials,nBits,useTSC);
		case 4:
			printf("Running safe prime test...\n");
			return safePrimeTest();
		case 5:
			printf("Running conversion test...\n");
			return conversionTest();
		case 6:
			printf("Running powmod fixed-base test...\n");
			return powmodFixedTest();
		case 7:
			printf("Running powmod fixed-base timing test...\n");
			printf("Fixed base results:\n");
			powmodTiming(30,1024,false,true);
			printf("Plain GMP results:\n");
			powmodTiming(30,1024,false,false);
			return 0;
		case 8:
			p192setup();
			printf("Running p192 reduction test...\n");
			printf("Special p192 results:\n");
			p192redTest(10000,true);
			printf("Plain mpz_mod results:\n");
			//p192redTest(10000,false);
			return 0;
		case 9:
			printf("Running montgomery multiplication test...\n");
			return mmulTest(100);
		case 10:
			printf("Running montgomery reduction test...\n");
			return mredTest(10,256);
			//return mredTest(1000,256);
		case 11:
			printf("Running montgomery reduction timing...\n");
			return mredTiming(50,1024,true);
		case 12:
			printf("Running plain reduction timing...\n");
			return plainredTiming(50,1024,true);
	}
}
